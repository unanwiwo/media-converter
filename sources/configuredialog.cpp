/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "configuredialog.hpp"
#include "info.hpp"
#include "exception.hpp"
#include "service.hpp"
#include "filewrapper.hpp"
#include "settingsmanager.hpp"
#include "settings.hpp"

#include "ui_configuredialog.h"

#include <QTimer>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>

namespace MediaConverter
{
    ConfigureDialog::ConfigureDialog(QWidget* parent):
        QDialog(parent),
        ui(new Ui::ConfigureDialog)
    {
        ui->setupUi(this);

        QTimer* timer = new QTimer(this);
        timer->setSingleShot(true);
        timer->start();

        connect(timer, SIGNAL(timeout()), this, SLOT(validate()));
        connect(ui->add_recursively_check_box, SIGNAL(toggled(bool)), timer, SLOT(start()));
        connect(ui->add_to_selection_check_box, SIGNAL(toggled(bool)), timer, SLOT(start()));
        connect(ui->simultaneous_conversions_spin_box, SIGNAL(valueChanged(int)), timer, SLOT(start()));
        connect(ui->program_line_edit, SIGNAL(textChanged(const QString&)), timer, SLOT(start()));
        connect(ui->reset_button, SIGNAL(clicked()), timer, SLOT(start()));
        connect(ui->apply_button, SIGNAL(clicked()), timer, SLOT(start()));

        reset();
    }

    void ConfigureDialog::validate()
    {
        const QSharedPointer<Settings> settings = settings_manager()->clone_settings();
        save(settings);

        const bool changed = *settings != *settings_manager()->get_settings();

        ui->reset_button->setEnabled(changed);
        ui->apply_button->setEnabled(changed);
    }

    void ConfigureDialog::autodetect_simultaneous_conversions()
    {
        ui->simultaneous_conversions_spin_box->setValue(SettingsManager::autodetect_simultaneous_conversions());
    }

    void ConfigureDialog::load_profile()
    {
        try {
            const QString path = QFileDialog::getOpenFileName(this, tr("Load profile"), settings_manager()->get_settings()->LastOpenProfilePath(), Info::get_profile_filters().join(";;"));
            if (!path.isEmpty()) {
                settings_manager()->get_settings()->SetLastOpenProfilePath(QFileInfo(path).absolutePath());

                const QJsonDocument document = QJsonDocument::fromJson(FileWrapper(path, QFile::ReadOnly).read());
                if (!document.isObject()) {
                    throw Exception(tr("Wrong profile format"));
                }

                const QJsonObject object = document.object();

                if (object.contains("program")) {
                    const QJsonValue program = object["program"];
                    if (program.isString()) {
                        ui->program_line_edit->setText(program.toString());
                    }
                }
            }
        } catch (const std::exception& error) {
            service()->error(error.what(), this);
        }
    }

    void ConfigureDialog::save_profile()
    {
        try {
            const QString path = QFileDialog::getSaveFileName(this, tr("Save profile"), settings_manager()->get_settings()->LastOpenProfilePath(), Info::get_profile_filters().join(";;"));
            if (!path.isEmpty()) {
                settings_manager()->get_settings()->SetLastOpenProfilePath(QFileInfo(path).absolutePath());
                QJsonObject object
                {
                    {"program", ui->program_line_edit->text()},
                };
                QJsonDocument document(object);
                FileWrapper file(path, QFile::WriteOnly);
                file.write(document.toJson());
            }
        } catch (const std::exception& error) {
            service()->error(error.what(), this);
        }
    }

    void ConfigureDialog::reset()
    {
        load(settings_manager()->get_settings());
    }

    void ConfigureDialog::apply()
    {
        save(settings_manager()->get_settings());
    }

    void ConfigureDialog::load(const QSharedPointer<Settings>& settings)
    {
        ui->add_recursively_check_box->setChecked(settings->AddRecursively());
        ui->add_to_selection_check_box->setChecked(settings->AddToSelection());
        ui->simultaneous_conversions_spin_box->setValue(settings->SimultaneousConversions());
        ui->program_line_edit->setText(settings->Format());
    }

    void ConfigureDialog::save(const QSharedPointer<Settings>& settings) const
    {
        settings->SetAddRecursively(ui->add_recursively_check_box->isChecked());
        settings->SetAddToSelection(ui->add_to_selection_check_box->isChecked());
        settings->SetSimultaneousConversions(ui->simultaneous_conversions_spin_box->value());
        settings->SetFormat(ui->program_line_edit->text());
    }
}
