/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "archive.hpp"
#include "exception.hpp"
#include "logging.hpp"

#include <QVector>
#include <QDir>

#include <archive.h>
#include <archive_entry.h>

using namespace MediaConverter;

namespace
{
    class ArchiveHandle
    {
    public:
        ArchiveHandle():
            arc(archive_read_new())
        {
            if (0 == arc) {
                throw Exception(QObject::tr("Can't allocate archive object"));
            }
        }

        ~ArchiveHandle()
        {
            archive_read_free(arc);
        }

        operator archive*() { return arc; }

    private:
        Q_DISABLE_COPY(ArchiveHandle)

    private:
        archive* arc;
    };

    class ArchiveReader
    {
    public:
        explicit ArchiveReader(const QString& path)
        {
            archive_read_support_filter_all(arc);
            archive_read_support_format_all(arc);

            if (ARCHIVE_OK != archive_read_open_filename(arc, qPrintable(path), 128 * 1024)) {
                throw Exception(QObject::tr("Error opening archive: %1 (%2)").arg(archive_errno(arc)).arg(archive_error_string(arc)));
            }
        }

        const ArchiveEntryPtr next()
        {
            archive_entry* arc_entry;
            int result = archive_read_next_header(arc, &arc_entry);

            if (ARCHIVE_EOF == result) {
                return ArchiveEntryPtr();
            }

            if (ARCHIVE_OK != result) {
                throw Exception(QObject::tr("Error fetching archive header: %1 (%2)").arg(archive_errno(arc)).arg(archive_error_string(arc)));
            }

            ArchiveEntryPtr entry(new ArchiveEntry);

            entry->path = QDir::cleanPath(archive_entry_pathname(arc_entry));

            QVector<char> buffer(archive_entry_size(arc_entry));
            entry->data.append(buffer.data(), archive_read_data(arc, buffer.data(), buffer.size()));

            return entry;
        }

    private:
        ArchiveHandle arc;
    };

    class ArchiveEntryFinder: public std::unary_function<ArchiveEntryPtr, bool>
    {
    public:
        explicit ArchiveEntryFinder(const QString& path): path(path) {}
        bool operator()(const ArchiveEntryPtr& entry) const
        {
            return entry->path == path;
        }
    private:
        QString path;
    };
}

namespace MediaConverter
{
    const ArchiveEntries extract_all(const QString& path)
    {
        ArchiveEntries entries;
        ArchiveReader reader(path);
        Q_FOREVER {
            ArchiveEntryPtr entry = reader.next();
            if (entry.isNull()) {
                break;
            }
            entries.append(entry);
        }
        return entries;
    }

    const ArchiveEntryPtr find(const ArchiveEntries& entries, const QString& path)
    {
        ArchiveEntries::ConstIterator iter = std::find_if(entries.constBegin(), entries.constEnd(), ArchiveEntryFinder(path));
        return entries.constEnd() != iter ? *iter : ArchiveEntryPtr();
    }
}
