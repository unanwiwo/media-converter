/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_TRANSLATION_HPP
#define MEDIA_CONVERTER_HEADER_TRANSLATION_HPP

#include "singleton.hpp"

#include <QLocale>
#include <QPixmap>
#include <QStringList>
#include <QSharedPointer>

namespace MediaConverter
{
    class Language;

    typedef QSharedPointer<Language> LanguagePtr;
    typedef QList<LanguagePtr> Languages;

    class Translation: public QObject, public Singleton<Translation>
    {
        Q_OBJECT

    public:
        Translation();

        bool install_default_language();
        bool install_system_language();
        bool install_language(const QString& name);

        const QString get_default_language_name() const;
        const QString get_system_language_name() const;
        const QStringList get_additional_language_names() const;
        const QString get_current_language_name() const { return current_language_name; }

        const QPixmap get_default_language_flag() const;
        const QPixmap get_additional_language_flag(const QString& name) const;

    Q_SIGNALS:
        void language_changed();

    private:
        void install_language(const LanguagePtr& language);
        void uninstall_all_languages();

    private:
        QLocale current_locale;
        QString current_language_name;

        LanguagePtr default_language;
        Languages additional_languages;
    };
}

#endif
