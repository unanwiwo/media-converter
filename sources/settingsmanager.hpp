/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_SETTINGS_MANAGER_HPP
#define MEDIA_CONVERTER_HEADER_SETTINGS_MANAGER_HPP

#include "singleton.hpp"
#include "settings.hpp"

namespace MediaConverter
{
    class SettingsManager: public Singleton<SettingsManager>
    {
    public:
        SettingsManager();
        ~SettingsManager();

        void set_settings(const SettingsPtr& settings) { app_settings = settings; }
        const SettingsPtr get_settings() const { return app_settings; }

        const SettingsPtr clone_settings() const;

        static size_t autodetect_simultaneous_conversions();

    private:
        QSettings qt_settings;
        SettingsPtr app_settings;
    };

    inline SettingsManager* settings_manager()
    {
        return SettingsManager::get_instance();
    }
}

#endif
