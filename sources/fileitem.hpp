﻿/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_FILE_ITEM_HPP
#define MEDIA_CONVERTER_HEADER_FILE_ITEM_HPP

#include "task.hpp"
#include "fileitemstate.hpp"

#include <QPointer>
#include <QFileInfo>
#include <QProgressDialog>
#include <QTreeWidgetItem>

namespace MediaConverter
{
    class FileItem: public QObject, public QTreeWidgetItem
    {
        Q_OBJECT

    public:
        enum Column
        {
            Column_Name,
            Column_Directory,
            //Column_SourceDirectory,
            //Column_SourceFormat,
            //Column_TargetDirectory,
            //Column_TargetFormat,
            Column_Status
        };

        enum Role
        {
            Role_ProgressValue = Qt::UserRole,
            Role_ProgressFormat,
            Role_ProgressStyleSheet
        };

        explicit FileItem(const QFileInfo& file_info);

        void translate();

        bool is_equivalent(const FileItem* other) const { return file_info == other->file_info; }

        bool is_ready() const { return state->is_ready(); }
        bool is_queued() const { return state->is_queued(); }
        bool is_running() const { return state->is_running(); }

        void queue();
        void start();
        void stop();

        void show_info_dialog();

    private Q_SLOTS:
        void do_finished();
        void do_completed();
        void do_progress_changed(int value);
        void do_statistics_changed(const QString& message);
        void do_error_occured();

    Q_SIGNALS:
        void task_finished();

    private:
        FileItemStatePtr state;
        QFileInfo file_info;
        QPointer<Task> task;
        QPointer<QProgressDialog> info_dialog;
        QPointer<QProgressBar> progress_bar;
    };
}

#endif
