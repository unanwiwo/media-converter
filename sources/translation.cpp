/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "translation.hpp"
#include "exception.hpp"
#include "logging.hpp"
#include "service.hpp"
#include "filewrapper.hpp"
#include "archive.hpp"
#include "settingsmanager.hpp"
#include "settings.hpp"

#include <QDir>
#include <QTranslator>
#include <QApplication>

namespace MediaConverter
{
    class Language
    {
    public:
        Language(const QByteArray& name, const QByteArray& flag, const QByteArray& qt_translation = QByteArray(), const QByteArray& app_translation = QByteArray()):
            name(name),
            qt_translation(qt_translation),
            app_translation(app_translation)
        {
            if (!this->qt_translation.isEmpty() && !qt_translator.load(static_cast<const uchar*>(static_cast<const void*>(this->qt_translation)), this->qt_translation.length())) {
                throw Exception(Translation::tr("Can't load qt translation file"));
            }
            if (!this->app_translation.isEmpty() && !app_translator.load(static_cast<const uchar*>(static_cast<const void*>(this->app_translation)), this->app_translation.length())) {
                throw Exception(Translation::tr("Can't load application translation file"));
            }
            if (!QRegExp("[a-z][a-z]_[A-Z][A-Z]").exactMatch(this->name)) {
                throw Exception(Translation::tr("Language name doesn't conform to ISO639_ISO3166"));
            }
            if (!this->flag.loadFromData(flag)) {
                throw Exception(Translation::tr("Can't load language flag"));
            }
        }

        void install_translator()
        {
            QApplication::installTranslator(&qt_translator);
            QApplication::installTranslator(&app_translator);
        }

        void remove_translator()
        {
            QApplication::removeTranslator(&app_translator);
            QApplication::removeTranslator(&qt_translator);
        }

        const QString& get_name() const { return name; }
        const QPixmap& get_flag() const { return flag; }

    private:
        QString name;
        QPixmap flag;
        QByteArray qt_translation;
        QByteArray app_translation;
        QTranslator qt_translator;
        QTranslator app_translator;
    };

    Translation::Translation():
        Singleton<Translation>(this)
    {
        try {
            default_language = LanguagePtr(new Language(FileWrapper(":/languages/english/name.txt", QFile::ReadOnly).read(),
                                                        FileWrapper(":/languages/english/flag.png", QFile::ReadOnly).read()));
        } catch (const std::exception& error) {
            service()->fatal(tr("Can't load default language: '%1'").arg(error.what()));
        }

        class EntryDataFinder
        {
        public:
            explicit EntryDataFinder(const ArchiveEntries& entries): entries(entries) {}
            const QByteArray operator()(const QString& path) const
            {
                ArchiveEntryPtr entry = find(entries, path);
                if (entry.isNull()) {
                    throw Exception(tr("Can't find archive entry '%1'").arg(path));
                }
                return entry->data;
            }
        private:
            ArchiveEntries entries;
        };

        const QDir language_directory(settings_manager()->get_settings()->LanguagePath(), "*.lng", QDir::Name, QDir::Files | QDir::Readable | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        if (language_directory.exists() && language_directory.isReadable()) {
            for (const QFileInfo& lng: language_directory.entryInfoList()) {
                try {
                    EntryDataFinder finder(extract_all(lng.filePath()));
                    additional_languages.append(LanguagePtr(new Language(finder(QString("%1/name.txt").arg(lng.baseName())),
                                                                         finder(QString("%1/flag.png").arg(lng.baseName())),
                                                                         finder(QString("%1/qtbase.qm").arg(lng.baseName())),
                                                                         finder(QString("%1/translation.qm").arg(lng.baseName())))));
                    log(tr("Loading language '%1'... ok").arg(lng.fileName()));
                } catch (const std::exception& error) {
                    log(tr("Loading language '%1'... failed: '%2'").arg(lng.fileName(), error.what()));
                }
            }
        } else {
            log(tr("Language directory '%1' doesn't exist or not readable").arg(language_directory.absolutePath()));
        }

        install_language(settings_manager()->get_settings()->Language()) || install_system_language() || install_default_language();
    }

    bool Translation::install_default_language()
    {
        uninstall_all_languages();
        install_language(default_language);
        log(tr("Installing language '%1'... ok").arg(default_language->get_name()));
        return true;
    }

    bool Translation::install_system_language()
    {
        return install_language(current_locale.name());
    }

    bool Translation::install_language(const QString& name)
    {
        if (get_default_language_name() == name) {
            return install_default_language();
        }
        Languages::Iterator iter = std::find_if(additional_languages.begin(), additional_languages.end(), [&name](const LanguagePtr& language){return language->get_name() == name;});
        if (additional_languages.end() == iter) {
            log(tr("Installing language '%1'... failed").arg(name));
            return false;
        }
        uninstall_all_languages();
        install_language(*iter);
        log(tr("Installing language '%1'... ok").arg(name));
        return true;
    }

    const QString Translation::get_default_language_name() const
    {
        return default_language->get_name();
    }

    const QString Translation::get_system_language_name() const
    {
        return current_locale.name();
    }

    const QStringList Translation::get_additional_language_names() const
    {
        QStringList additional_language_names;
        std::transform(additional_languages.constBegin(), additional_languages.constEnd(), std::back_inserter(additional_language_names), [](const LanguagePtr& language){return language->get_name();});
        return additional_language_names;
    }

    const QPixmap Translation::get_default_language_flag() const
    {
        return default_language->get_flag();
    }

    const QPixmap Translation::get_additional_language_flag(const QString& name) const
    {
        if (get_default_language_name() == name) {
            return get_default_language_flag();
        }
        Languages::ConstIterator iter = std::find_if(additional_languages.constBegin(), additional_languages.constEnd(), [&name](const LanguagePtr& language){return language->get_name() == name;});
        if (additional_languages.constEnd() == iter) {
            log(tr("Can't find language '%1'").arg(name));
            return QPixmap();
        }
        return (*iter)->get_flag();
    }

    void Translation::install_language(const LanguagePtr& language)
    {
        language->install_translator();
        current_language_name = language->get_name();
        settings_manager()->get_settings()->SetLanguage(current_language_name);
        Q_EMIT language_changed();
    }

    void Translation::uninstall_all_languages()
    {
        default_language->remove_translator();
        QList<Language*> languages;
        std::transform(additional_languages.begin(), additional_languages.end(), std::back_inserter(languages), std::mem_fun_ref(&LanguagePtr::data));
        std::for_each(languages.begin(), languages.end(), std::mem_fun(&Language::remove_translator));
    }
}
