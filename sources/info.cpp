/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "info.hpp"

#include <QObject>

namespace MediaConverter
{
namespace Info
{
    const QString get_app_name()
    {
        return QObject::tr("Media Converter");
    }

    const QString get_app_internal_name()
    {
        return "media-converter";
    }

    const QString get_app_version()
    {
        return SCONSX_VERSION_STR;
    }

    const QString get_app_architecture()
    {
        return SCONSX_ARCHITECTURE_STR;
    }

    const QString get_app_dev_years()
    {
        return "2007-2019";
    }

    const QString get_app_domain()
    {
        return "https://gitlab.com/ykurganov/media-converter";
    }

    const QString get_author_name()
    {
        return QObject::tr("Yanis Kurganov");
    }

    const QString get_author_internal_name()
    {
        return "yanis-kurganov";
    }

    const QString get_author_email()
    {
        return "yanis.kurganov@gmail.com";
    }

    const QString get_author_domain()
    {
        return "https://gitlab.com/ykurganov";
    }

    const QStringList get_media_wildcards()
    {
        return QStringList() << "*.flac" << "*.ogg" << "*.ape" << "*.alac" << "*.tta" << "*.ofr" << "*.wv" << "*.wav" << "*.wavex" << "*.mp3" << "*.pam" << "*.aiff" << "*.au" << "*.avr" << "*.caf" << "*.htk" << "*.iff" << "*.mat4" << "*.mat5" << "*.paf" << "*.pvf" << "*.raw" << "*.sd2" << "*.sds" << "*.sf" << "*.voc" << "*.w64" << "*.wve" << "*.xi";
    }

    const QStringList get_media_filters()
    {
        return QStringList() << QObject::tr("Media files (%1)").arg(get_media_wildcards().join(" ")) << QObject::tr("All files (*)");
    }

    const QStringList get_program_filters()
    {
        return QStringList() << QObject::tr("All files (*)");
    }

    const QStringList get_profile_wildcards()
    {
        return QStringList() << "*.json";
    }

    const QStringList get_profile_filters()
    {
        return QStringList() << QObject::tr("Profile files (%1)").arg(get_profile_wildcards().join(" "));
    }
}
}
