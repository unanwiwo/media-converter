/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_SERVICE_HPP
#define MEDIA_CONVERTER_HEADER_SERVICE_HPP

#include "singleton.hpp"

#include <QString>
#include <QWidget>

namespace MediaConverter
{
    class Service: public Singleton<Service>
    {
    public:
        Service();

        void information(const QString& message, QWidget* parent = nullptr);
        void warning(const QString& message, QWidget* parent = nullptr);
        void error(const QString& message, QWidget* parent = nullptr);
        void critical(const QString& message, QWidget* parent = nullptr);
        void fatal(const QString& message, QWidget* parent = nullptr);
    };

    inline Service* service()
    {
        return Service::get_instance();
    }
}

#endif
