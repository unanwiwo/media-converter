/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "fileitemcreator.hpp"
#include "info.hpp"

#include <QCoreApplication>

namespace
{
    const QList<QTreeWidgetItem*> items_cast(const QList<MediaConverter::FileItem*>& items)
    {
        QList<QTreeWidgetItem*> qitems;
        std::copy(items.constBegin(), items.constEnd(), std::back_inserter(qitems));
        return qitems;
    }
}

namespace MediaConverter
{
    FileItemCreator::FileItemCreator(const QStringList& entries, QThread* target_thread, bool recursively):
        entries(entries),
        target_thread(target_thread),
        filters(QDir::Files | QDir::Readable | QDir::NoSymLinks | QDir::NoDotAndDotDot),
        wildcards(Info::get_media_wildcards()),
        cancel_request(false)
    {
        if (recursively) {
            filters |= QDir::AllDirs;
        }

        connect(&statistics_timer, SIGNAL(timeout()), this, SLOT(emit_progress_changed()));
        statistics_timer.start(250);
    }

    bool FileItemCreator::is_canceled() const
    {
        QMutexLocker mutex_locker(&mutex);
        return cancel_request;
    }

    void FileItemCreator::cancel()
    {
        QMutexLocker mutex_locker(&mutex);
        cancel_request = true;
    }

    void FileItemCreator::run()
    {
        QFileInfoList entry_infos;
        std::copy(entries.constBegin(), entries.constEnd(), std::back_inserter(entry_infos));
        add_entries(entry_infos);
        Q_EMIT about_to_finish();
        if (is_canceled()) {
            qDeleteAll(items);
            Q_EMIT canceled();
        } else {
            std::for_each(items.constBegin(), items.constEnd(), [this](FileItem* item){item->moveToThread(target_thread);});
            Q_EMIT completed(items_cast(items));
        }
    }

    void FileItemCreator::emit_progress_changed()
    {
        Q_EMIT progress_changed(items.size());
        Q_EMIT progress_changed(tr("%1<br>%2 files processed").arg(current_dir.length() <= 33 ? current_dir : current_dir.left(15) + "..." + current_dir.right(15)).arg(items.size()));
    }

    void FileItemCreator::add_entries(const QFileInfoList& entry_infos)
    {
        if (is_canceled()) {
            return;
        }

        QFileInfoList dir_infos;
        QFileInfoList file_infos;

        std::remove_copy_if(entry_infos.constBegin(), entry_infos.constEnd(), std::back_inserter(dir_infos), std::not1(std::mem_fun_ref(&QFileInfo::isDir)));
        std::remove_copy_if(entry_infos.constBegin(), entry_infos.constEnd(), std::back_inserter(file_infos), std::not1(std::mem_fun_ref(&QFileInfo::isFile)));

        add_dirs(dir_infos);
        add_files(file_infos);

        QCoreApplication::processEvents();
    }

    void FileItemCreator::add_dirs(const QFileInfoList& dir_infos)
    {
        for (const QFileInfo& dir_info: dir_infos) {
            current_dir = dir_info.absoluteFilePath();
            add_entries(QDir(dir_info.filePath()).entryInfoList(wildcards, filters));
        }
    }

    void FileItemCreator::add_files(const QFileInfoList& file_infos)
    {
        std::transform(file_infos.constBegin(), file_infos.constEnd(), std::back_inserter(items), [](const QFileInfo& file_info){return new FileItem(file_info);});
    }
}
