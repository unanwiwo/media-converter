/*
 * This file is part of Media Converter.
 *
 * Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
 * Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_CONVERTER_HEADER_MAIN_WINDOW_HPP
#define MEDIA_CONVERTER_HEADER_MAIN_WINDOW_HPP

#include <QMainWindow>
#include <QActionGroup>
#include <QSharedPointer>

namespace Ui
{
    class MainWindow;
}

namespace MediaConverter
{
    class MainWindow: public QMainWindow
    {
        Q_OBJECT

    public:
        MainWindow();
        ~MainWindow();

    private Q_SLOTS:
        void translate();

        void show_history();

        void change_tool_button_style(int style);
        void change_language();

        void add_files();
        void add_directory();

        void configure();
        void about();

    protected:
        virtual void closeEvent(QCloseEvent* event);

    private:
        QSharedPointer<Ui::MainWindow> ui;
        QActionGroup* language_action_group;
    };
}

#endif
