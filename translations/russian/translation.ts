<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name></name>
    <message>
        <source>Ready</source>
        <translation type="obsolete">Готов</translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <source>About</source>
        <comment>Dialog title</comment>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>&lt;p&gt;%1 %2&lt;br&gt;
Copyright (C) %3 %4&lt;/p&gt;
&lt;p&gt;%5&lt;/p&gt;</source>
        <comment>%1 - program name; %2 - program version; %3 - development years; %4 - author&apos;s name; %5 - site</comment>
        <translation type="obsolete">&lt;p&gt;%1 %2&lt;br&gt;
Авторское право (C) %3 %4&lt;/p&gt;
&lt;p&gt;%5&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;%1 %2&lt;br&gt;
Copyright (C) %3 %4&lt;/p&gt;
&lt;p&gt;%5&lt;/p&gt;</source>
        <comment>%1 - program name; %2 - program version; %3 - development years; %4 - author name; %5 - site</comment>
        <translation type="obsolete">&lt;p&gt;%1 %2&lt;br&gt;
Авторское право (C) %3 %4&lt;/p&gt;
&lt;p&gt;%5&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;%1 %2&lt;br&gt;
Copyright (C) %3 %4&lt;/p&gt;
&lt;p&gt;%5&lt;/p&gt;</source>
        <comment>%1 - application name; %2 - application version; %3 - development years; %4 - author name; %5 - application website</comment>
        <translation type="obsolete">&lt;p&gt;%1 %2&lt;br&gt;
Авторское право (C) %3 %4&lt;/p&gt;
&lt;p&gt;%5&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>AboutFlacDialog</name>
    <message>
        <source>Features</source>
        <translation type="vanished">Возможности</translation>
    </message>
    <message>
        <source>Goals</source>
        <translation type="vanished">Цели</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Лицензия</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3, but lossless, meaning that audio is compressed in FLAC without any loss in quality. This is similar to how Zip works, except with FLAC you will get much better compression because it is designed specifically for audio, and you can play back compressed FLAC files in your favorite player (or your car or home stereo) just like you would an MP3 file.&lt;/p&gt;&lt;p&gt;FLAC supports tagging, cover art, and fast seeking. FLAC is freely available and supported on most operating systems, including Windows, &quot;unix&quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2, and Amiga.&lt;/p&gt;&lt;p&gt;When we say that FLAC is &quot;Free&quot; it means more than just that it is available at no cost. It means that the specification of the format is fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance), and that neither the FLAC format nor any of the implemented encoding/decoding methods are covered by any known patent. It also means that all the source code is available under open-source licenses. It is the first truly open and free lossless audio format.&lt;/p&gt;</source>
        <comment>Introduction of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;FLAC - это аббревиатура от Free Lossless Audio Codec (свободный аудиокодек, сжимающий без потерь). Проект FLAC включает:&lt;ul&gt;&lt;li&gt;потоковый формат;&lt;/li&gt;&lt;li&gt;базовые кодеры и декодеры в виде библиотек;&lt;/li&gt;&lt;li&gt;&lt;tt&gt;flac&lt;/tt&gt;, утилиту командной строки, выполняющую сжатие и распаковку файлов .flac;&lt;/li&gt;&lt;li&gt;&lt;tt&gt;metaflac&lt;/tt&gt;, утилиту командной строки для редактирования метаданных в файлах .flac;&lt;/li&gt;&lt;li&gt;плагины для разных плейеров.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;FLAC компилируется на множестве платформ: Windows, &quot;unix&quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2 и Amiga. Имеются системы сборки для autoconf/automake, MSVC, Watcom C и Project Builder.&lt;/p&gt;&lt;p&gt;&quot;Свобода&quot; означает, что спецификация потокового формата открыта для всех и может быть использована для любых целей (проект FLAC оставляет за собой право устанавливать спецификации и сертифицировать относящиеся к нему продукты на совместимость), а также то, что ни формат, ни один из реализованных методов кодирования/декодирования не запатентованы. Это также значит, что все исходные тексты доступны по лицензиям, обязывающим предоставлять исходные коды.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Notable features of FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Lossless:&lt;/b&gt; The encoding of audio (PCM) data incurs no loss of information, and the decoded audio is bit-for-bit identical to what went into the encoder. Each frame contains a 16-bit CRC of the frame data for detecting transmission errors. The integrity of the audio data is further insured by storing an MD5 signature of the original unencoded audio data in the file header, which can be compared against later during decoding or testing.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Fast:&lt;/b&gt; FLAC is asymmetric in favor of decode speed. Decoding requires only integer arithmetic, and is much less compute-intensive than for most perceptual codecs. Real-time decode performance is easily achievable on even modest hardware.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Hardware support:&lt;/b&gt; FLAC is supported by dozens of consumer electronic devices, from portable players, to home stereo equipment, to car stereo.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Flexible metadata:&lt;/b&gt; FLAC&apos;s metadata system supports tags, cover art, seek tables, and cue sheets. Applications can write their own APPLICATION metadata once they register an ID. New metadata blocks can be defined and implemented in future versions of FLAC without breaking older streams or decoders.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Seekable:&lt;/b&gt; FLAC supports fast sample-accurate seeking. Not only is this useful for playback, it makes FLAC files suitable for use in editing applications.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Streamable:&lt;/b&gt; Each FLAC frame contains enough data to decode that frame. FLAC does not even rely on previous or following frames. FLAC uses sync codes and CRCs (similar to MPEG and other formats), which, along with framing, allow decoders to pick up in the middle of a stream with a minimum of delay.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Suitable for archiving:&lt;/b&gt; FLAC is an open format, and there is no generation loss if you need to convert your data to another format in the future. In addition to the frame CRCs and MD5 signature, flac has a verify option that decodes the encoded stream in parallel with the encoding process and compares the result to the original, aborting with an error if there is a mismatch.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Convenient CD archiving:&lt;/b&gt; FLAC has a &quot;cue sheet&quot; metadata block for storing a CD table of contents and all track and index points. For instance, you can rip a CD to a single file, then import the CD&apos;s extracted cue sheet while encoding to yield a single file representation of the entire CD. If your original CD is damaged, the cue sheet can be exported later in order to burn an exact copy.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Error resistant:&lt;/b&gt; Because of FLAC&apos;s framing, stream errors limit the damage to the frame in which the error occurred, typically a small fraction of a second worth of data. Contrast this with some other lossless codecs, in which a single error destroys the remainder of the stream.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;What FLAC is &lt;b&gt;not&lt;/b&gt;:&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <comment>Features of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;Характеристики FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Сжатие без потерь:&lt;/b&gt; Кодирование PCM данных не приводит к потере информации, следовательно декодируемый аудиофайл абсолютно идентичен тому, который был подан на вход кодеру. Чтобы определить возможные ошибки при передаче файла, для каждого фрейма вычисляется 16-битная контрольная сумма. Целостность на дальнейшем этапе подтверждается подписью MD5 распакованных данных, которая находится в заголовке и может быть проверена при воспроизведении, декодировании или с помощью тестирования.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Скорость:&lt;/b&gt; Скорость работы при кодировании и декодировании несимметричны. Для декодирования используется только целочисленная арифметика, которая требует значительно меньше вычислений, чем в перцепционных кодеках. Декодирование в реальном времени легко достижимо даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Аппаратная поддержка:&lt;/b&gt; Благодаря свободной базовой реализации и простому декодированию FLAC является единственным аудиокодеком, сжимающим без потерь, который имеет аппаратную поддержку.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Метаданные:&lt;/b&gt; FLAC имеет расширяемую систему метаданных. Новые блоки метаданных могут быть определены и реализованы в будущих версиях без потери обратной совместимости. Сейчас определены типы метаданных для таблиц поиска, тегов и списков разметки аудиодисков. Приложение может использовать блок метаданных &lt;tt&gt;APPLICATION&lt;/tt&gt; после регистрации для него &lt;tt&gt;ID&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поиск:&lt;/b&gt; FLAC поддерживает быстрый и точный поиск, что полезно не только при воспроизведении, но и дает возможность использовать FLAC в звуковых редакторах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поточность:&lt;/b&gt; Каждый фрейм FLAC содержит достаточно информации для собственного декодирования. Текущий фрейм FLAC не зависит от предыдущих и последующих. FLAC использует коды синхронизации и контрольные суммы, что позволяет декодеру быстро выбирать позицию в текущем потоке.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование:&lt;/b&gt; FLAC удобно использовать для архивации, так как сжатие с его помощью не приводит к потере информации. Если в будущем Вы решите использовать другой формат, данные будут восстановлены из .flac файла в первоначальном виде. Кроме контрольной суммы фрейма и подписи MD5, утилита &lt;tt&gt;flac&lt;/tt&gt; имеет возможность проверки, использование которой приводит к тому, что кодируемый поток сразу же декодируется и сравнивается с исходным. Если происходит ошибка, кодер прекращает работу.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование аудиодисков:&lt;/b&gt; У FLAC если блок метаданных &quot;cue sheet&quot;, в котором сохраняется таблица разметки аудиодиска. Например, можно записать аудиодиск в один файл, а затем импортировать его таблицу разметки при кодировании, чтобы полученный файл имел такое же представление как и диск. Если оригинал аудиодиска будет испорчен, то вы сможете восстановить таблицу разметки, чтобы записать точную копию диска.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Устойчивость от ошибок:&lt;/b&gt; Благодаря разбиению на фреймы, ошибки в потоке локализуются до уровня фрейма, в котором произошла ошибка (обычно несколько сотых секунды). В некоторых кодеках одна ошибка может привести к потере всего остатка потока.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;Чего &lt;b&gt;нет&lt;/b&gt; во FLAC:&lt;ul&gt;&lt;li&gt;FLAC не осуществляет сжатие с потерями. Для этого существует много хороших форматов, таких как Ogg Vorbis, MPC и MP3 (отличная реализация с открытими исходными текстами LAME).&lt;/li&gt;&lt;li&gt;FLAC не будет SDMI совместимым и т.п. Перед проектом не стоит цели поддерживать методы защиты, которые на практике лишь увеличивают объем файла. Конечно, мы не сможем препятствовать кому-либо создавать несвободные блоки метаданных, однако, стандартные декодеры все равно будут их пропускать.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Since FLAC is an open-source project, it&apos;s important to have a set of goals that everyone works to. They may change slightly from time to time but they&apos;re a good guideline. Changes should be in line with the goals and should not attempt to embrace any of the anti-goals.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC should be and stay an open format with an open-source reference implementation.&lt;/li&gt;&lt;li&gt;FLAC should be lossless. This seems obvious but lossy compression seems to creep into every audio codec. This goal also means that &lt;tt&gt;flac&lt;/tt&gt; should stay archival quality and be truly lossless for all input. Testing of releases should be thorough.&lt;/li&gt;&lt;li&gt;FLAC should yield respectable compression, on par or better than other lossless codecs.&lt;/li&gt;&lt;li&gt;FLAC should allow at least realtime decoding on even modest hardware.&lt;/li&gt;&lt;li&gt;FLAC should support fast sample-accurate seeking.&lt;/li&gt;&lt;li&gt;FLAC should allow gapless playback of consecutive streams. This follows from the lossless goal.&lt;/li&gt;&lt;li&gt;The FLAC project owes a lot to the many people who have advanced the audio compression field so freely, and aims also to contribute through the open-source development of new ideas.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Anti-goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <comment>Goals of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;Так как FLAC - это открытый проект, важно определить список целей, к чему нужно стремиться. Время от времени они могут немного изменяться, но всегда должны определять направление развития. изменения должны согласовываться с текущими целями и не пытаться включить в себя антицели.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Цели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC должен оставаться открытым форматом. Все исходные тексты либо под LGPL, либо под GPL.&lt;/li&gt;&lt;li&gt;FLAC должен производить только сжатие без потерь. Вроде бы это очевидно, однако, кодирование с потерями пытается проникнуть во все аудио кодеки. Эта цель также означает, что FLAC должен придерживаться только принципов архивирования и сжимать без потерь абсолютно все типы входных данных. Релизы должны тщательно тестироваться.&lt;/li&gt;&lt;li&gt;FLAC должен достичь приемлимого уровня сжатия файлов.&lt;/li&gt;&lt;li&gt;FLAC должен иметь низкие аппаратные требования и обеспечить декодирование в реальном времени даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать быстрый и точный поиск.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать воспроизведение без пауз для следующих друг за другом потоков.&lt;/li&gt;&lt;li&gt;Проект FLAC находится в долгу перед многими людьми, кто улучшал методы сжатия звука, и нацелен на поддержку новых идей с помощью открытой разработки.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Антицели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Сжатие с потерями. Существует достаточно много хороших форматов, предназначенных именно для этого (Ogg Vorbis, MP3, и т.д.).&lt;/li&gt;&lt;li&gt;Защита от копирования в любом виде.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC is a free codec in the fullest sense. This page explicitly states all that you may do with the format and software.&lt;/p&gt;&lt;p&gt;The FLAC and Ogg FLAC formats themselves, and their specifications, are fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance). They are free for commercial or noncommercial use. That means that commercial developers may independently write FLAC or Ogg FLAC software which is compatible with the specifications for no charge and without restrictions of any kind. There are no licensing fees of any kind for use of the formats or their specifications, or for distributing, selling, or streaming media in the FLAC or Ogg FLAC formats.&lt;/p&gt;&lt;p&gt;The FLAC project also makes available software that implements the formats, which is distributed according to Open Source licenses as follows:&lt;/p&gt;&lt;p&gt;The reference implementation libraries are licensed under Xiph&apos;s variant of the BSD License, a.k.a. the Xiph License. This is a trivial variation of the popular BSD License where the Xiph.org Foundation is substituted for the University of California, Berkeley. In simple terms, these libraries may be used by any application, Open or proprietary, linked or incorporated in whole, so long as acknowledgement is made to Xiph.org Foundation when using the source code in whole or in derived works. The Xiph License is free enough that the libraries have been used in commercial products to implement FLAC, including in the firmware of hardware devices where other Open Source licenses can be problematic. In the source code these libraries are called libFLAC and libFLAC++.&lt;/p&gt;&lt;p&gt;The rest of the software that the FLAC project provides is licensed under the GNU General Public License (GPL). This software includes various utilities for converting files to and from FLAC format, plugins for audio players, et cetera. In general, the GPL allows redistribution as long as derived works are also made available in source code form according to compatible terms.&lt;/p&gt;&lt;p&gt;Neither the FLAC nor Ogg FLAC formats nor any of the implemented encoding/decoding methods are covered by any known patent.&lt;/p&gt;&lt;p&gt;FLAC is one of a family of codecs of the Xiph.org Foundation, all created according to the same free ideals. For some other codecs&apos; descriptions of the Xiph License see the Speex and Vorbis license pages.&lt;/p&gt;&lt;p&gt;If you would like to redistribute parts or all of FLAC under different terms, contact Josh Coalson.&lt;/p&gt;</source>
        <comment>License of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;...&lt;/p&gt;&lt;p&gt;FLAC - свободный кодек в самом полном смысле этого слова. Ниже описано то, что Вы можете сделать с форматом и программным обеспечением.&lt;/p&gt;&lt;p&gt;FLAC и Ogg FLAC форматы непосредственно, и их спецификации, являются полностью открытыми, могут использоваться для любой цели (проект FLAC оставляет за собой право установить спецификацию FLAC). Они бесплатные для коммерческого или некоммерческого использования. Это означает, что коммерческие разработчики могут независимо написать FLAC или Ogg FLAC программное обеспечение, которое является совместимым со спецификациями без ограничений любого вида. Нет никаких лицензионных сборов для использования форматов или их спецификаций, для распространения, продажи.&lt;/p&gt;&lt;p&gt;Ни FLAC, ни Ogg FLAC форматы, ни любой из осуществленных методов кодирования/расшифровки не закрыты никаким известным патентом.&lt;/p&gt;&lt;p&gt;Если Вы хотели бы распространить части или весь FLAC согласно другим условиям, свяжитесь с &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Джошем Коалсоном&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;...&lt;/p&gt;</translation>
    </message>
    <message>
        <source>About FLAC</source>
        <translation type="obsolete">О FLAC</translation>
    </message>
    <message>
        <source>Introduction</source>
        <translation type="vanished">Вступление</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3, but lossless, meaning that audio is compressed in FLAC without any loss in quality. This is similar to how Zip works, except with FLAC you will get much better compression because it is designed specifically for audio, and you can play back compressed FLAC files in your favorite player (or your car or home stereo) just like you would an MP3 file.&lt;/p&gt;
&lt;p&gt;FLAC supports tagging, cover art, and fast seeking. FLAC is freely available and supported on most operating systems, including Windows, &quot;unix&quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2, and Amiga.&lt;/p&gt;
&lt;p&gt;When we say that FLAC is &quot;Free&quot; it means more than just that it is available at no cost. It means that the specification of the format is fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance), and that neither the FLAC format nor any of the implemented encoding/decoding methods are covered by any known patent. It also means that all the source code is available under open-source licenses. It is the first truly open and free lossless audio format. (For more information, see the license page.)&lt;/p&gt;
&lt;p&gt;See %1 for more information.&lt;/p&gt;</source>
        <comment>Introduction of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;FLAC - это аббревиатура от Free Lossless Audio Codec (свободный аудиокодек, сжимающий без потерь). Проект FLAC включает:&lt;ul&gt;
&lt;li&gt;потоковый формат;&lt;/li&gt;
&lt;li&gt;базовые кодеры и декодеры в виде библиотек;&lt;/li&gt;
&lt;li&gt;&lt;tt&gt;flac&lt;/tt&gt;, утилиту командной строки, выполняющую сжатие и распаковку файлов .flac;&lt;/li&gt;
&lt;li&gt;&lt;tt&gt;metaflac&lt;/tt&gt;, утилиту командной строки для редактирования метаданных в файлах .flac;&lt;/li&gt;
&lt;li&gt;плагины для разных плейеров.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;FLAC компилируется на множестве платформ: Windows, &quot;unix&quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2 и Amiga. Имеются системы сборки для autoconf/automake, MSVC, Watcom C и Project Builder.&lt;/p&gt;
&lt;p&gt;&quot;Свобода&quot; означает, что спецификация потокового формата открыта для всех и может быть использована для любых целей (проект FLAC оставляет за собой право устанавливать спецификации и сертифицировать относящиеся к нему продукты на совместимость), а также то, что ни формат, ни один из реализованных методов кодирования/декодирования не запатентованы. Это также значит, что все исходные тексты доступны по лицензиям, обязывающим предоставлять исходные коды.&lt;/p&gt;
&lt;p&gt;См. %1 для получения более подробной информации.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Notable features of FLAC:&lt;ul&gt;
&lt;li&gt;&lt;b&gt;Lossless:&lt;/b&gt; The encoding of audio (PCM) data incurs no loss of information, and the decoded audio is bit-for-bit identical to what went into the encoder. Each frame contains a 16-bit CRC of the frame data for detecting transmission errors. The integrity of the audio data is further insured by storing an MD5 signature of the original unencoded audio data in the file header, which can be compared against later during decoding or testing.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Fast:&lt;/b&gt; FLAC is asymmetric in favor of decode speed. Decoding requires only integer arithmetic, and is much less compute-intensive than for most perceptual codecs. Real-time decode performance is easily achievable on even modest hardware.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Hardware support:&lt;/b&gt; FLAC is supported by dozens of consumer electronic devices, from portable players, to home stereo equipment, to car stereo.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Flexible metadata:&lt;/b&gt; FLAC&apos;s metadata system supports tags, cover art, seek tables, and cue sheets. Applications can write their own APPLICATION metadata once they register an ID. New metadata blocks can be defined and implemented in future versions of FLAC without breaking older streams or decoders.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Seekable:&lt;/b&gt; FLAC supports fast sample-accurate seeking. Not only is this useful for playback, it makes FLAC files suitable for use in editing applications.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Streamable:&lt;/b&gt; Each FLAC frame contains enough data to decode that frame. FLAC does not even rely on previous or following frames. FLAC uses sync codes and CRCs (similar to MPEG and other formats), which, along with framing, allow decoders to pick up in the middle of a stream with a minimum of delay.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Suitable for archiving:&lt;/b&gt; FLAC is an open format, and there is no generation loss if you need to convert your data to another format in the future. In addition to the frame CRCs and MD5 signature, flac has a verify option that decodes the encoded stream in parallel with the encoding process and compares the result to the original, aborting with an error if there is a mismatch.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Convenient CD archiving:&lt;/b&gt; FLAC has a &quot;cue sheet&quot; metadata block for storing a CD table of contents and all track and index points. For instance, you can rip a CD to a single file, then import the CD&apos;s extracted cue sheet while encoding to yield a single file representation of the entire CD. If your original CD is damaged, the cue sheet can be exported later in order to burn an exact copy.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Error resistant:&lt;/b&gt; Because of FLAC&apos;s framing, stream errors limit the damage to the frame in which the error occurred, typically a small fraction of a second worth of data. Contrast this with some other lossless codecs, in which a single error destroys the remainder of the stream.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;What FLAC is &lt;b&gt;not&lt;/b&gt;:&lt;ul&gt;
&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;
&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</source>
        <comment>Features of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;Характеристики FLAC:&lt;ul&gt;
&lt;li&gt;&lt;b&gt;Сжатие без потерь:&lt;/b&gt; Кодирование PCM данных не приводит к потере информации, следовательно декодируемый аудиофайл абсолютно идентичен тому, который был подан на вход кодеру. Чтобы определить возможные ошибки при передаче файла, для каждого фрейма вычисляется 16-битная контрольная сумма. Целостность на дальнейшем этапе подтверждается подписью MD5 распакованных данных, которая находится в заголовке и может быть проверена при воспроизведении, декодировании или с помощью тестирования.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Скорость:&lt;/b&gt; Скорость работы при кодировании и декодировании несимметричны. Для декодирования используется только целочисленная арифметика, которая требует значительно меньше вычислений, чем в перцепционных кодеках. Декодирование в реальном времени легко достижимо даже на старых компьютерах.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Аппаратная поддержка:&lt;/b&gt; Благодаря свободной базовой реализации и простому декодированию FLAC является единственным аудиокодеком, сжимающим без потерь, который имеет аппаратную поддержку.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Метаданные:&lt;/b&gt; FLAC имеет расширяемую систему метаданных. Новые блоки метаданных могут быть определены и реализованы в будущих версиях без потери обратной совместимости. Сейчас определены типы метаданных для таблиц поиска, тегов и списков разметки аудиодисков. Приложение может использовать блок метаданных &lt;tt&gt;APPLICATION&lt;/tt&gt; после регистрации для него &lt;tt&gt;ID&lt;/tt&gt;.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Поиск:&lt;/b&gt; FLAC поддерживает быстрый и точный поиск, что полезно не только при воспроизведении, но и дает возможность использовать FLAC в звуковых редакторах.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Поточность:&lt;/b&gt; Каждый фрейм FLAC содержит достаточно информации для собственного декодирования. Текущий фрейм FLAC не зависит от предыдущих и последующих. FLAC использует коды синхронизации и контрольные суммы, что позволяет декодеру быстро выбирать позицию в текущем потоке.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Архивирование:&lt;/b&gt; FLAC удобно использовать для архивации, так как сжатие с его помощью не приводит к потере информации. Если в будущем Вы решите использовать другой формат, данные будут восстановлены из .flac файла в первоначальном виде. Кроме контрольной суммы фрейма и подписи MD5, утилита &lt;tt&gt;flac&lt;/tt&gt; имеет возможность проверки, использование которой приводит к тому, что кодируемый поток сразу же декодируется и сравнивается с исходным. Если происходит ошибка, кодер прекращает работу.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Архивирование аудиодисков:&lt;/b&gt; У FLAC если блок метаданных &quot;cue sheet&quot;, в котором сохраняется таблица разметки аудиодиска. Например, можно записать аудиодиск в один файл, а затем импортировать его таблицу разметки при кодировании, чтобы полученный файл имел такое же представление как и диск. Если оригинал аудиодиска будет испорчен, то вы сможете восстановить таблицу разметки, чтобы записать точную копию диска.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Устойчивость от ошибок:&lt;/b&gt; Благодаря разбиению на фреймы, ошибки в потоке локализуются до уровня фрейма, в котором произошла ошибка (обычно несколько сотых секунды). В некоторых кодеках одна ошибка может привести к потере всего остатка потока.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Чего &lt;b&gt;нет&lt;/b&gt; во FLAC:&lt;ul&gt;
&lt;li&gt;FLAC не осуществляет сжатие с потерями. Для этого существует много хороших форматов, таких как Ogg Vorbis, MPC и MP3 (отличная реализация с открытими исходными текстами LAME).&lt;/li&gt;
&lt;li&gt;FLAC не будет SDMI совместимым и т.п. Перед проектом не стоит цели поддерживать методы защиты, которые на практике лишь увеличивают объем файла. Конечно, мы не сможем препятствовать кому-либо создавать несвободные блоки метаданных, однако, стандартные декодеры все равно будут их пропускать.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Since FLAC is an open-source project, it&apos;s important to have a set of goals that everyone works to. They may change slightly from time to time but they&apos;re a good guideline. Changes should be in line with the goals and should not attempt to embrace any of the anti-goals.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Goals&lt;/b&gt;&lt;ul&gt;
&lt;li&gt;FLAC should be and stay an open format with an open-source reference implementation.&lt;/li&gt;
&lt;li&gt;FLAC should be lossless. This seems obvious but lossy compression seems to creep into every audio codec. This goal also means that &lt;tt&gt;flac&lt;/tt&gt; should stay archival quality and be truly lossless for all input. Testing of releases should be thorough.&lt;/li&gt;
&lt;li&gt;FLAC should yield respectable compression, on par or better than other lossless codecs.&lt;/li&gt;
&lt;li&gt;FLAC should allow at least realtime decoding on even modest hardware.&lt;/li&gt;
&lt;li&gt;FLAC should support fast sample-accurate seeking.&lt;/li&gt;
&lt;li&gt;FLAC should allow gapless playback of consecutive streams. This follows from the lossless goal.&lt;/li&gt;
&lt;li&gt;The FLAC project owes a lot to the many people who have advanced the audio compression field so freely, and aims also to contribute through the open-source development of new ideas.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Anti-goals&lt;/b&gt;&lt;ul&gt;
&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;
&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</source>
        <comment>Goals of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;Так как FLAC - это открытый проект, важно определить список целей, к чему нужно стремиться. Время от времени они могут немного изменяться, но всегда должны определять направление развития. изменения должны согласовываться с текущими целями и не пытаться включить в себя антицели.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Цели&lt;/b&gt;&lt;ul&gt;
&lt;li&gt;FLAC должен оставаться открытым форматом. Все исходные тексты либо под LGPL, либо под GPL.&lt;/li&gt;
&lt;li&gt;FLAC должен производить только сжатие без потерь. Вроде бы это очевидно, однако, кодирование с потерями пытается проникнуть во все аудио кодеки. Эта цель также означает, что FLAC должен придерживаться только принципов архивирования и сжимать без потерь абсолютно все типы входных данных. Релизы должны тщательно тестироваться.&lt;/li&gt;
&lt;li&gt;FLAC должен достичь приемлимого уровня сжатия файлов.&lt;/li&gt;
&lt;li&gt;FLAC должен иметь низкие аппаратные требования и обеспечить декодирование в реальном времени даже на старых компьютерах.&lt;/li&gt;
&lt;li&gt;FLAC должен поддерживать быстрый и точный поиск.&lt;/li&gt;
&lt;li&gt;FLAC должен поддерживать воспроизведение без пауз для следующих друг за другом потоков.&lt;/li&gt;
&lt;li&gt;Проект FLAC находится в долгу перед многими людьми, кто улучшал методы сжатия звука, и нацелен на поддержку новых идей с помощью открытой разработки.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Антицели&lt;/b&gt;&lt;ul&gt;
&lt;li&gt;Сжатие с потерями. Существует достаточно много хороших форматов, предназначенных именно для этого (Ogg Vorbis, MP3, и т.д.).&lt;/li&gt;
&lt;li&gt;Защита от копирования в любом виде.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC is a free codec in the fullest sense. This page explicitly states all that you may do with the format and software.&lt;/p&gt;
&lt;p&gt;The FLAC and Ogg FLAC formats themselves, and their specifications, are fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance). They are free for commercial or noncommercial use. That means that commercial developers may independently write FLAC or Ogg FLAC software which is compatible with the specifications for no charge and without restrictions of any kind. There are no licensing fees of any kind for use of the formats or their specifications, or for distributing, selling, or streaming media in the FLAC or Ogg FLAC formats.&lt;/p&gt;
&lt;p&gt;The FLAC project also makes available software that implements the formats, which is distributed according to Open Source licenses as follows:&lt;/p&gt;
&lt;p&gt;The reference implementation libraries are licensed under Xiph&apos;s variant of the BSD License, a.k.a. the Xiph License. This is a trivial variation of the popular BSD License where the Xiph.org Foundation is substituted for the University of California, Berkeley. In simple terms, these libraries may be used by any application, Open or proprietary, linked or incorporated in whole, so long as acknowledgement is made to Xiph.org Foundation when using the source code in whole or in derived works. The Xiph License is free enough that the libraries have been used in commercial products to implement FLAC, including in the firmware of hardware devices where other Open Source licenses can be problematic. In the source code these libraries are called libFLAC and libFLAC++.&lt;/p&gt;
&lt;p&gt;The rest of the software that the FLAC project provides is licensed under the GNU General Public License (GPL). This software includes various utilities for converting files to and from FLAC format, plugins for audio players, et cetera. In general, the GPL allows redistribution as long as derived works are also made available in source code form according to compatible terms.&lt;/p&gt;
&lt;p&gt;Neither the FLAC nor Ogg FLAC formats nor any of the implemented encoding/decoding methods are covered by any known patent.&lt;/p&gt;
&lt;p&gt;FLAC is one of a family of codecs of the Xiph.org Foundation, all created according to the same free ideals. For some other codecs&apos; descriptions of the Xiph License see the Speex and Vorbis license pages.&lt;/p&gt;
&lt;p&gt;If you would like to redistribute parts or all of FLAC under different terms, &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;contact Josh Coalson&lt;/a&gt;.&lt;/p&gt;</source>
        <comment>License of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;...&lt;/p&gt;
&lt;p&gt;FLAC - свободный кодек в самом полном смысле этого слова. Ниже описано то, что Вы можете сделать с форматом и программным обеспечением.&lt;/p&gt;
&lt;p&gt;FLAC и Ogg FLAC форматы непосредственно, и их спецификации, являются полностью открытыми, могут использоваться для любой цели (проект FLAC оставляет за собой право установить спецификацию FLAC). Они бесплатные для коммерческого или некоммерческого использования. Это означает, что коммерческие разработчики могут независимо написать FLAC или Ogg FLAC программное обеспечение, которое является совместимым со спецификациями без ограничений любого вида. Нет никаких лицензионных сборов для использования форматов или их спецификаций, для распространения, продажи.&lt;/p&gt;
&lt;p&gt;Ни FLAC, ни Ogg FLAC форматы, ни любой из осуществленных методов кодирования/расшифровки не закрыты никаким известным патентом.&lt;/p&gt;
&lt;p&gt;Если Вы хотели бы распространить части или весь FLAC согласно другим условиям, свяжитесь с &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Джошем Коалсоном&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;...&lt;/p&gt;</translation>
    </message>
    <message>
        <source>with support for Ogg FLAC</source>
        <comment>Part of the technical information of FLAC</comment>
        <translation type="obsolete">с поддержкой Ogg FLAC</translation>
    </message>
    <message>
        <source>without support for Ogg FLAC</source>
        <comment>Part of the technical information of FLAC</comment>
        <translation type="obsolete">без поддержки Ogg FLAC</translation>
    </message>
    <message>
        <source>&lt;p&gt;Technical Information&lt;br&gt;
Using %1&lt;br&gt;
FLAC API %2 (%3)&lt;br&gt;
FLAC++ API %4&lt;/p&gt;</source>
        <comment>The various technical information of FLAC (%1 - vendor; %2, %4 - API versions; %3 - support for Ogg FLAC)</comment>
        <translation type="obsolete">&lt;p&gt;Техническая информация&lt;br&gt;
Используется %1&lt;br&gt;
FLAC API %2 (%3)&lt;br&gt;
FLAC++ API %4&lt;/p&gt;</translation>
    </message>
    <message>
        <source>About FLAC</source>
        <comment>Dialog title</comment>
        <translation type="vanished">О библиотеке FLAC</translation>
    </message>
</context>
<context>
    <name>AboutFlacDialogImpl</name>
    <message>
        <source>Features</source>
        <translation type="obsolete">Возможности</translation>
    </message>
    <message>
        <source>Goals</source>
        <translation type="obsolete">Цели</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="obsolete">Лицензия</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>&lt;p&gt;Notable features of FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Lossless:&lt;/b&gt; The encoding of audio (PCM) data incurs no loss of information, and the decoded audio is bit-for-bit identical to what went into the encoder. Each frame contains a 16-bit CRC of the frame data for detecting transmission errors. The integrity of the audio data is further insured by storing an MD5 signature of the original unencoded audio data in the file header, which can be compared against later during decoding or testing.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Fast:&lt;/b&gt; FLAC is asymmetric in favor of decode speed. Decoding requires only integer arithmetic, and is much less compute-intensive than for most perceptual codecs. Real-time decode performance is easily achievable on even modest hardware.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Hardware support:&lt;/b&gt; FLAC is supported by dozens of consumer electronic devices, from portable players, to home stereo equipment, to car stereo.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Flexible metadata:&lt;/b&gt; FLAC&apos;s metadata system supports tags, cover art, seek tables, and cue sheets. Applications can write their own APPLICATION metadata once they register an ID. New metadata blocks can be defined and implemented in future versions of FLAC without breaking older streams or decoders.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Seekable:&lt;/b&gt; FLAC supports fast sample-accurate seeking. Not only is this useful for playback, it makes FLAC files suitable for use in editing applications.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Streamable:&lt;/b&gt; Each FLAC frame contains enough data to decode that frame. FLAC does not even rely on previous or following frames. FLAC uses sync codes and CRCs (similar to MPEG and other formats), which, along with framing, allow decoders to pick up in the middle of a stream with a minimum of delay.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Suitable for archiving:&lt;/b&gt; FLAC is an open format, and there is no generation loss if you need to convert your data to another format in the future. In addition to the frame CRCs and MD5 signature, flac has a verify option that decodes the encoded stream in parallel with the encoding process and compares the result to the original, aborting with an error if there is a mismatch.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Convenient CD archiving:&lt;/b&gt; FLAC has a &quot;cue sheet&quot; metadata block for storing a CD table of contents and all track and index points. For instance, you can rip a CD to a single file, then import the CD&apos;s extracted cue sheet while encoding to yield a single file representation of the entire CD. If your original CD is damaged, the cue sheet can be exported later in order to burn an exact copy.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Error resistant:&lt;/b&gt; Because of FLAC&apos;s framing, stream errors limit the damage to the frame in which the error occurred, typically a small fraction of a second worth of data. Contrast this with some other lossless codecs, in which a single error destroys the remainder of the stream.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;What FLAC is &lt;b&gt;not&lt;/b&gt;:&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <comment>Features of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;Характеристики FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Сжатие без потерь:&lt;/b&gt; Кодирование PCM данных не приводит к потере информации, следовательно декодируемый аудиофайл абсолютно идентичен тому, который был подан на вход кодеру. Чтобы определить возможные ошибки при передаче файла, для каждого фрейма вычисляется 16-битная контрольная сумма. Целостность на дальнейшем этапе подтверждается подписью MD5 распакованных данных, которая находится в заголовке и может быть проверена при воспроизведении, декодировании или с помощью тестирования.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Скорость:&lt;/b&gt; Скорость работы при кодировании и декодировании несимметричны. Для декодирования используется только целочисленная арифметика, которая требует значительно меньше вычислений, чем в перцепционных кодеках. Декодирование в реальном времени легко достижимо даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Аппаратная поддержка:&lt;/b&gt; Благодаря свободной базовой реализации и простому декодированию FLAC является единственным аудиокодеком, сжимающим без потерь, который имеет аппаратную поддержку.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Метаданные:&lt;/b&gt; FLAC имеет расширяемую систему метаданных. Новые блоки метаданных могут быть определены и реализованы в будущих версиях без потери обратной совместимости. Сейчас определены типы метаданных для таблиц поиска, тегов и списков разметки аудиодисков. Приложение может использовать блок метаданных &lt;tt&gt;APPLICATION&lt;/tt&gt; после регистрации для него &lt;tt&gt;ID&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поиск:&lt;/b&gt; FLAC поддерживает быстрый и точный поиск, что полезно не только при воспроизведении, но и дает возможность использовать FLAC в звуковых редакторах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поточность:&lt;/b&gt; Каждый фрейм FLAC содержит достаточно информации для собственного декодирования. Текущий фрейм FLAC не зависит от предыдущих и последующих. FLAC использует коды синхронизации и контрольные суммы, что позволяет декодеру быстро выбирать позицию в текущем потоке.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование:&lt;/b&gt; FLAC удобно использовать для архивации, так как сжатие с его помощью не приводит к потере информации. Если в будущем Вы решите использовать другой формат, данные будут восстановлены из .flac файла в первоначальном виде. Кроме контрольной суммы фрейма и подписи MD5, утилита &lt;tt&gt;flac&lt;/tt&gt; имеет возможность проверки, использование которой приводит к тому, что кодируемый поток сразу же декодируется и сравнивается с исходным. Если происходит ошибка, кодер прекращает работу.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование аудиодисков:&lt;/b&gt; У FLAC если блок метаданных &quot;cue sheet&quot;, в котором сохраняется таблица разметки аудиодиска. Например, можно записать аудиодиск в один файл, а затем импортировать его таблицу разметки при кодировании, чтобы полученный файл имел такое же представление как и диск. Если оригинал аудиодиска будет испорчен, то вы сможете восстановить таблицу разметки, чтобы записать точную копию диска.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Устойчивость от ошибок:&lt;/b&gt; Благодаря разбиению на фреймы, ошибки в потоке локализуются до уровня фрейма, в котором произошла ошибка (обычно несколько сотых секунды). В некоторых кодеках одна ошибка может привести к потере всего остатка потока.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;Чего &lt;b&gt;нет&lt;/b&gt; во FLAC:&lt;ul&gt;&lt;li&gt;FLAC не осуществляет сжатие с потерями. Для этого существует много хороших форматов, таких как Ogg Vorbis, MPC и MP3 (отличная реализация с открытими исходными текстами LAME).&lt;/li&gt;&lt;li&gt;FLAC не будет SDMI совместимым и т.п. Перед проектом не стоит цели поддерживать методы защиты, которые на практике лишь увеличивают объем файла. Конечно, мы не сможем препятствовать кому-либо создавать несвободные блоки метаданных, однако, стандартные декодеры все равно будут их пропускать.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Since FLAC is an open-source project, it&apos;s important to have a set of goals that everyone works to. They may change slightly from time to time but they&apos;re a good guideline. Changes should be in line with the goals and should not attempt to embrace any of the anti-goals.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC should be and stay an open format with an open-source reference implementation.&lt;/li&gt;&lt;li&gt;FLAC should be lossless. This seems obvious but lossy compression seems to creep into every audio codec. This goal also means that &lt;tt&gt;flac&lt;/tt&gt; should stay archival quality and be truly lossless for all input. Testing of releases should be thorough.&lt;/li&gt;&lt;li&gt;FLAC should yield respectable compression, on par or better than other lossless codecs.&lt;/li&gt;&lt;li&gt;FLAC should allow at least realtime decoding on even modest hardware.&lt;/li&gt;&lt;li&gt;FLAC should support fast sample-accurate seeking.&lt;/li&gt;&lt;li&gt;FLAC should allow gapless playback of consecutive streams. This follows from the lossless goal.&lt;/li&gt;&lt;li&gt;The FLAC project owes a lot to the many people who have advanced the audio compression field so freely, and aims also to contribute through the open-source development of new ideas.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Anti-goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <comment>Goals of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;Так как FLAC - это открытый проект, важно определить список целей, к чему нужно стремиться. Время от времени они могут немного изменяться, но всегда должны определять направление развития. изменения должны согласовываться с текущими целями и не пытаться включить в себя антицели.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Цели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC должен оставаться открытым форматом. Все исходные тексты либо под LGPL, либо под GPL.&lt;/li&gt;&lt;li&gt;FLAC должен производить только сжатие без потерь. Вроде бы это очевидно, однако, кодирование с потерями пытается проникнуть во все аудио кодеки. Эта цель также означает, что FLAC должен придерживаться только принципов архивирования и сжимать без потерь абсолютно все типы входных данных. Релизы должны тщательно тестироваться.&lt;/li&gt;&lt;li&gt;FLAC должен достичь приемлимого уровня сжатия файлов.&lt;/li&gt;&lt;li&gt;FLAC должен иметь низкие аппаратные требования и обеспечить декодирование в реальном времени даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать быстрый и точный поиск.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать воспроизведение без пауз для следующих друг за другом потоков.&lt;/li&gt;&lt;li&gt;Проект FLAC находится в долгу перед многими людьми, кто улучшал методы сжатия звука, и нацелен на поддержку новых идей с помощью открытой разработки.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Антицели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Сжатие с потерями. Существует достаточно много хороших форматов, предназначенных именно для этого (Ogg Vorbis, MP3, и т.д.).&lt;/li&gt;&lt;li&gt;Защита от копирования в любом виде.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC is a free codec in the fullest sense. This page explicitly states all that you may do with the format and software.&lt;/p&gt;&lt;p&gt;The FLAC and Ogg FLAC formats themselves, and their specifications, are fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance). They are free for commercial or noncommercial use. That means that commercial developers may independently write FLAC or Ogg FLAC software which is compatible with the specifications for no charge and without restrictions of any kind. There are no licensing fees of any kind for use of the formats or their specifications, or for distributing, selling, or streaming media in the FLAC or Ogg FLAC formats.&lt;/p&gt;&lt;p&gt;The FLAC project also makes available software that implements the formats, which is distributed according to Open Source licenses as follows:&lt;/p&gt;&lt;p&gt;The reference implementation libraries are licensed under Xiph&apos;s variant of the BSD License, a.k.a. the Xiph License. This is a trivial variation of the popular BSD License where the Xiph.org Foundation is substituted for the University of California, Berkeley. In simple terms, these libraries may be used by any application, Open or proprietary, linked or incorporated in whole, so long as acknowledgement is made to Xiph.org Foundation when using the source code in whole or in derived works. The Xiph License is free enough that the libraries have been used in commercial products to implement FLAC, including in the firmware of hardware devices where other Open Source licenses can be problematic. In the source code these libraries are called libFLAC and libFLAC++.&lt;/p&gt;&lt;p&gt;The rest of the software that the FLAC project provides is licensed under the GNU General Public License (GPL). This software includes various utilities for converting files to and from FLAC format, plugins for audio players, et cetera. In general, the GPL allows redistribution as long as derived works are also made available in source code form according to compatible terms.&lt;/p&gt;&lt;p&gt;Neither the FLAC nor Ogg FLAC formats nor any of the implemented encoding/decoding methods are covered by any known patent.&lt;/p&gt;&lt;p&gt;FLAC is one of a family of codecs of the Xiph.org Foundation, all created according to the same free ideals. For some other codecs&apos; descriptions of the Xiph License see the Speex and Vorbis license pages.&lt;/p&gt;&lt;p&gt;If you would like to redistribute parts or all of FLAC under different terms, contact Josh Coalson.&lt;/p&gt;</source>
        <comment>License of FLAC</comment>
        <translation type="obsolete">&lt;p&gt;...&lt;/p&gt;&lt;p&gt;FLAC - свободный кодек в самом полном смысле этого слова. Ниже описано то, что Вы можете сделать с форматом и программным обеспечением.&lt;/p&gt;&lt;p&gt;FLAC и Ogg FLAC форматы непосредственно, и их спецификации, являются полностью открытыми, могут использоваться для любой цели (проект FLAC оставляет за собой право установить спецификацию FLAC). Они бесплатные для коммерческого или некоммерческого использования. Это означает, что коммерческие разработчики могут независимо написать FLAC или Ogg FLAC программное обеспечение, которое является совместимым со спецификациями без ограничений любого вида. Нет никаких лицензионных сборов для использования форматов или их спецификаций, для распространения, продажи.&lt;/p&gt;&lt;p&gt;Ни FLAC, ни Ogg FLAC форматы, ни любой из осуществленных методов кодирования/расшифровки не закрыты никаким известным патентом.&lt;/p&gt;&lt;p&gt;Если Вы хотели бы распространить части или весь FLAC согласно другим условиям, свяжитесь с &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Джошем Коалсоном&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;...&lt;/p&gt;</translation>
    </message>
    <message>
        <source>with support for Ogg FLAC</source>
        <comment>Technical information of FLAC: support for Ogg FLAC</comment>
        <translation type="obsolete">с поддержкой Ogg FLAC</translation>
    </message>
    <message>
        <source>without support for Ogg FLAC</source>
        <comment>Technical information of FLAC: support for Ogg FLAC</comment>
        <translation type="obsolete">без поддержки Ogg FLAC</translation>
    </message>
    <message>
        <source>with support for Ogg FLAC</source>
        <comment>Technical information of FLAC (support for Ogg FLAC)</comment>
        <translation type="obsolete">с поддержкой Ogg FLAC</translation>
    </message>
    <message>
        <source>without support for Ogg FLAC</source>
        <comment>Technical information of FLAC (support for Ogg FLAC)</comment>
        <translation type="obsolete">без поддержки Ogg FLAC</translation>
    </message>
</context>
<context>
    <name>ApplicationErrorDialog</name>
    <message>
        <source>Internal program error</source>
        <translation type="obsolete">Внутренняя ошибка в программе</translation>
    </message>
    <message>
        <source>Terminate</source>
        <translation type="obsolete">Завершить</translation>
    </message>
    <message>
        <source>Program log...</source>
        <translation type="obsolete">Лог программы...</translation>
    </message>
    <message>
        <source>Save report</source>
        <translation type="obsolete">Сохранить отчёт</translation>
    </message>
    <message>
        <source>&lt;p&gt;Internal program error.&lt;/p&gt;
&lt;p&gt;Please, send the error report to the developer.&lt;br&gt;
You will help to make this program better.&lt;/p&gt;</source>
        <comment>Message text</comment>
        <translation type="obsolete">&lt;p&gt;Внутренняя ошибка в программе.&lt;/p&gt;
&lt;p&gt;Пожалуйста, отошлите отчёт об ошибке разработчику.&lt;br&gt;
Вы поможете сделать эту программу лучше.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Program log</source>
        <comment>Dialog title</comment>
        <translation type="obsolete">Лог программы</translation>
    </message>
    <message>
        <source>Save error report</source>
        <comment>Save File Name Dialog title</comment>
        <translation type="obsolete">Сохранить отчёт об ошибке</translation>
    </message>
    <message>
        <source>Internal program error</source>
        <comment>Dialog title</comment>
        <translation type="obsolete">Внутренняя ошибка в программе</translation>
    </message>
    <message>
        <source>Program log</source>
        <translation type="obsolete">Лог программы</translation>
    </message>
    <message>
        <source>Can&apos;t save error report file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно сохранить файл отчёта об ошибке &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Save error report</source>
        <translation type="obsolete">Сохранить отчёт об ошибке</translation>
    </message>
</context>
<context>
    <name>AudioConverter::ConfigDialog</name>
    <message>
        <source>Where is the utility</source>
        <translation type="vanished">Выбор утилиты конвертации</translation>
    </message>
</context>
<context>
    <name>AudioConverter::ConfigureDialog</name>
    <message>
        <source>Open Program</source>
        <translation type="vanished">Найти программу</translation>
    </message>
    <message>
        <source>Open Preset</source>
        <translation type="vanished">Загрузить настройки</translation>
    </message>
    <message>
        <source>Find Program</source>
        <translation type="vanished">Найти программу</translation>
    </message>
    <message>
        <source>Preset Files (*.json)</source>
        <translation type="vanished">Файлы настроек (*.json)</translation>
    </message>
    <message>
        <source>Load Preset</source>
        <translation type="vanished">Загрузить настройки</translation>
    </message>
    <message>
        <source>Save Preset</source>
        <translation type="vanished">Сохранить настройки</translation>
    </message>
    <message>
        <source>Find program</source>
        <translation type="vanished">Найти программу</translation>
    </message>
    <message>
        <source>Load profile</source>
        <translation type="vanished">Загрузить профиль</translation>
    </message>
    <message>
        <source>Save profile</source>
        <translation type="vanished">Сохранить профиль</translation>
    </message>
</context>
<context>
    <name>AudioConverter::FileItem</name>
    <message>
        <source>Ready</source>
        <translation type="vanished">Готов</translation>
    </message>
    <message>
        <source>No statistics available</source>
        <translation type="vanished">Нет данных</translation>
    </message>
    <message>
        <source>Queued</source>
        <translation type="vanished">В очереди</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Закрыть</translation>
    </message>
    <message>
        <source>Working...</source>
        <translation type="vanished">Конвертация...</translation>
    </message>
    <message>
        <source>Finished</source>
        <translation type="vanished">Завершено</translation>
    </message>
    <message>
        <source>Stopped</source>
        <translation type="vanished">Остановлено</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="vanished">Завершено</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Ошибка</translation>
    </message>
</context>
<context>
    <name>AudioConverter::FileItemCreator</name>
    <message>
        <source>%1&lt;br&gt;%2 files processed</source>
        <translation type="vanished">%1&lt;br&gt;%2 файлов обработано</translation>
    </message>
</context>
<context>
    <name>AudioConverter::FileItemList</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Название</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation type="obsolete">Директория</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Размер</translation>
    </message>
    <message>
        <source>Modified</source>
        <translation type="obsolete">Изменён</translation>
    </message>
    <message>
        <source>Progress</source>
        <translation type="obsolete">Выполнено</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Состояние</translation>
    </message>
    <message>
        <source>Adding files...</source>
        <translation type="vanished">Добавление файлов...</translation>
    </message>
    <message>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to remove?</source>
        <translation type="vanished">Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите удалить их?</translation>
    </message>
    <message>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to clear?</source>
        <translation type="vanished">Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите удалить их?</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Предупреждение</translation>
    </message>
    <message>
        <source>Are you sure you want to add %1 files?&lt;br&gt;This may decrease performance considerably.</source>
        <translation type="vanished">Вы действительно хотите добавить %1 файлов?&lt;br&gt;Производительность программы при этом может сильно пострадать.</translation>
    </message>
</context>
<context>
    <name>AudioConverter::Item</name>
    <message>
        <source>No statistics available: the job was not started yet</source>
        <translation type="vanished">Конвертация ещё не проводилась</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="vanished">Готов</translation>
    </message>
    <message>
        <source>No statistics available</source>
        <translation type="vanished">Конвертация ещё не проводилась</translation>
    </message>
    <message>
        <source>Queued</source>
        <translation type="vanished">В очереди</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Закрыть</translation>
    </message>
    <message>
        <source>Working...</source>
        <translation type="vanished">Конвертация...</translation>
    </message>
    <message>
        <source>Stopped</source>
        <translation type="vanished">Остановлено</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="vanished">Завершено</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Ошибка</translation>
    </message>
</context>
<context>
    <name>AudioConverter::ItemCreator</name>
    <message>
        <source>%1&lt;br&gt;%2 files processed</source>
        <translation type="vanished">%1&lt;br&gt;%2 файлов обработано</translation>
    </message>
</context>
<context>
    <name>AudioConverter::ItemList</name>
    <message>
        <source>Directory</source>
        <translation type="vanished">Директория</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Название</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Размер</translation>
    </message>
    <message>
        <source>Modified</source>
        <translation type="vanished">Изменён</translation>
    </message>
    <message>
        <source>Progress</source>
        <translation type="vanished">Выполнено</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">Состояние</translation>
    </message>
    <message>
        <source>Adding files...</source>
        <translation type="vanished">Добавление файлов...</translation>
    </message>
    <message>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to remove?</source>
        <translation type="vanished">Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите удалить эти задачи?</translation>
    </message>
    <message>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to clear?</source>
        <translation type="vanished">Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите удалить все задачи?</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Предупреждение</translation>
    </message>
    <message>
        <source>Are you sure you want to add %1 files?&lt;br&gt;This may decrease performance considerably.</source>
        <translation type="vanished">Вы действительно хотите добавить %1 файлов?&lt;br&gt;Производительность программы при этом может сильно пострадать.</translation>
    </message>
    <message>
        <source>Are you sure you want to try to add %1 files?&lt;br&gt;It will reduce program&apos;s performance considerably!</source>
        <translation type="vanished">Вы действительно хотите добавить %1 файлов?&lt;br&gt;Производительность программы при этом может сильно пострадать!</translation>
    </message>
</context>
<context>
    <name>AudioConverter::MainWindow</name>
    <message>
        <source>Change language to &apos;%1&apos;</source>
        <translation type="vanished">Изменить язык на &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t install language &apos;%1&apos;</source>
        <translation type="vanished">Невозможно установить язык &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Add Files</source>
        <translation type="vanished">Добавить файлы</translation>
    </message>
    <message>
        <source>Add Directory</source>
        <translation type="vanished">Добавить директорию</translation>
    </message>
    <message>
        <source>Add files</source>
        <translation type="vanished">Добавить файлы</translation>
    </message>
    <message>
        <source>Add directory</source>
        <translation type="vanished">Добавить директорию</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">О программе</translation>
    </message>
    <message>
        <source>%1 %2&lt;br&gt;Copyright (C) %3 %4&lt;br&gt;&lt;a href=%5&gt;%5&lt;/a&gt;&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;&lt;br&gt;</source>
        <translation type="vanished">%1 %2&lt;br&gt;Авторское право (C) %3 %4&lt;br&gt;&lt;a href=%5&gt;%5&lt;/a&gt;&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to exit?</source>
        <translation type="vanished">Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите выйти?</translation>
    </message>
    <message>
        <source>%1 %2&lt;br&gt;Copyright (C) %3 %4&lt;br&gt;&lt;a href=&amp;quot;http://%5/&amp;quot;&gt;%5&lt;/a&gt;&lt;br&gt;&lt;a href=&amp;quot;http://%6/&amp;quot;&gt;%6&lt;/a&gt;&lt;br&gt;</source>
        <translation type="vanished">%1 %2&lt;br&gt;Авторское право (C) %3 %4&lt;br&gt;&lt;a href=&amp;quot;http://%5/&amp;quot;&gt;%5&lt;/a&gt;&lt;br&gt;&lt;a href=&amp;quot;http://%6/&amp;quot;&gt;%6&lt;/a&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>There are unfinished tasks in the list.&lt;br&gt;Are you sure you want to exit (and stop them immediately)?</source>
        <translation type="vanished">Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите остановить все процессы конвертации и выйти?</translation>
    </message>
</context>
<context>
    <name>AudioConverter::StatusBar</name>
    <message>
        <source>Ready</source>
        <translation type="vanished">Готов</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <source>Configuration</source>
        <translation type="vanished">Настройка</translation>
    </message>
    <message>
        <source>Enable/Disable recursive directory adding to the filelist</source>
        <translation type="vanished">Добавлять файлы из директории и всех поддиректорий</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Настройки</translation>
    </message>
    <message>
        <source>Force overwriting of output files</source>
        <translation type="vanished">Заменять файлы если они уже существуют на диске</translation>
    </message>
    <message>
        <source>Force Overwrite</source>
        <translation type="vanished">Заменять существующие файлы</translation>
    </message>
    <message>
        <source>Force Overwrite Output Files</source>
        <translation type="vanished">Заменять существующие файлы</translation>
    </message>
    <message>
        <source>Whether to add directories recursively</source>
        <translation type="vanished">Добавлять файлы из директории и всех поддиректорий</translation>
    </message>
    <message>
        <source>Add Directories Recursively</source>
        <translation type="vanished">Рекурсивный обход директорий</translation>
    </message>
    <message>
        <source>Whether to select new files</source>
        <translation type="vanished">Автоматически выделять добавляемые файлы</translation>
    </message>
    <message>
        <source>Add To Selection</source>
        <translation type="vanished">Выделять добавляемые файлы</translation>
    </message>
    <message>
        <source>How many files can be converted simultaneously</source>
        <translation type="vanished">Количество одновременно запущенных конвертаций</translation>
    </message>
    <message>
        <source>Simultaneous Conversions: </source>
        <translation type="vanished">Одновременных конвертаций: </translation>
    </message>
    <message>
        <source>Path to the conversion program</source>
        <translation type="vanished">Пусть к программе конвертации</translation>
    </message>
    <message>
        <source>Arguments:</source>
        <translation type="vanished">Аргументы:</translation>
    </message>
    <message>
        <source>Arguments of the conversion program</source>
        <translation type="vanished">Аргументы программы конвертации</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Utility:</source>
        <translation type="vanished">Утилита:</translation>
    </message>
    <message>
        <source>Options:</source>
        <translation type="vanished">Параметры:</translation>
    </message>
</context>
<context>
    <name>ConfigureDialog</name>
    <message>
        <location filename="../../forms/configuredialog.ui" line="15"/>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="25"/>
        <source>Whether to add directories recursively</source>
        <translation>Добавлять файлы из директорий и всех их поддиректорий</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="28"/>
        <source>Add Directories Recursively</source>
        <translation>Рекурсивный обход директорий</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="35"/>
        <source>Whether to select new files</source>
        <translation>Автоматически выделять добавляемые файлы</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="38"/>
        <source>Add To Selection</source>
        <translation>Выделять добавляемые файлы</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="47"/>
        <location filename="../../forms/configuredialog.ui" line="60"/>
        <source>Maximum number of files &quot;under conversion&quot; that the software allows. It&apos;s recommended to use the number of processors on the local machine. For example, specify 4 on a 4-core machine.</source>
        <translation>Количество одновременно конвертируемых файлов. Рекомендуется использовать количество ядер в процессоре. Например, если у Вас процессор с 4-мя ядрами, то укажите 4.</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="50"/>
        <source>Si&amp;multaneous conversions:</source>
        <translation>Одновременных &amp;конвертаций:</translation>
    </message>
    <message>
        <source>Ar&amp;guments:</source>
        <translation type="vanished">&amp;Аргументы:</translation>
    </message>
    <message>
        <source>Returns an absolute path including the file name: c:/foo/bar.rol.zed =&gt; c:/foo/bar.rol.zed</source>
        <translation type="vanished">Возвращает абсолютный путь до файла: c:/foo/bar.rol.zed =&gt; c:/foo/bar.rol.zed</translation>
    </message>
    <message>
        <source>Returns the name of the file without the path: c:/foo/bar.rol.zed =&gt; bar.rol.zed</source>
        <translation type="vanished">Возвращает название файла без пути: c:/foo/bar.rol.zed =&gt; bar.rol.zed</translation>
    </message>
    <message>
        <source>Use this channel for logging and progress information</source>
        <translation type="vanished">Использовать этот канал для логирования и вычисления прогресса</translation>
    </message>
    <message>
        <source>Channel for grabbing logging and progress information</source>
        <translation type="vanished">Канал для захвата журнала и информации о прогрессе</translation>
    </message>
    <message>
        <source>Read Channel</source>
        <translation type="vanished">Канал для чтения</translation>
    </message>
    <message>
        <source>The standard error (stderr)</source>
        <translation type="vanished">Стандартный вывод ошибок (stderr)</translation>
    </message>
    <message>
        <source>Standard Error</source>
        <translation type="vanished">Стандартный вывод ошибок</translation>
    </message>
    <message>
        <source>The standard output (stdout)</source>
        <translation type="vanished">Стандартный вывод (stdout)</translation>
    </message>
    <message>
        <source>Standard Output</source>
        <translation type="vanished">Стандартный вывод</translation>
    </message>
    <message>
        <source>Merge stderr and stdout into one channel. The stderr and stdout data are interleaved</source>
        <translation type="obsolete">Стандартный вывод ошибок и стандартный вывод, объединённые в один канал. Данные каналов чередуются</translation>
    </message>
    <message>
        <source>The standard error and the standard output merged into one channel. Their data are interleaved</source>
        <translation type="obsolete">Стандартный вывод ошибок и стандартный вывод, объединённые в один канал. Данные каналов чередуются</translation>
    </message>
    <message>
        <source>The standard error and standard output merged into one channel. The standard error and standard output data are interleaved</source>
        <translation type="vanished">Стандартный вывод ошибок и стандартный вывод, объединённые в один канал. Данные каналов чередуются</translation>
    </message>
    <message>
        <source>Merged</source>
        <translation type="vanished">Объединённый</translation>
    </message>
    <message>
        <source>&amp;Arguments:</source>
        <translation type="vanished">&amp;Аргументы:</translation>
    </message>
    <message>
        <source>The brief base name of the file without the path. The brief base name consists of all characters in the file up to (but not including) the first dot character: c:/foo/bar.rol.zed =&gt; bar</source>
        <translation type="vanished">Краткое название файла без расширения: c:/foo/bar.rol.zed =&gt; bar</translation>
    </message>
    <message>
        <source>The full base name of the file without the path. The full base name consists of all characters in the file up to (but not including) the last dot character: c:/foo/bar.rol.zed =&gt; bar.rol</source>
        <translation type="vanished">Полное название файла без расширения: c:/foo/bar.rol.zed =&gt; bar.rol</translation>
    </message>
    <message>
        <source>The brief extension of the file. The brief extension consists of all characters in the file after (but not including) the last dot character: c:/foo/bar.rol.zed =&gt; zed</source>
        <translation type="vanished">Краткое расширение файла: c:/foo/bar.rol.zed =&gt; zed</translation>
    </message>
    <message>
        <source>The full extension of the file. The full extension consists of all characters in the file after (but not including) the first dot character: c:/foo/bar.rol.zed =&gt; rol.zed</source>
        <translation type="vanished">Полное расширение файла: c:/foo/bar.rol.zed =&gt; rol.zed</translation>
    </message>
    <message>
        <source>An absolute path including the file name: c:/foo/bar.rol.zed =&gt; c:/foo/bar.rol.zed</source>
        <translation type="vanished">Абсолютный путь до файла: c:/foo/bar.rol.zed =&gt; c:/foo/bar.rol.zed</translation>
    </message>
    <message>
        <source>The name of the file without the path: c:/foo/bar.rol.zed =&gt; bar.rol.zed</source>
        <translation type="vanished">Название файла без пути: c:/foo/bar.rol.zed =&gt; bar.rol.zed</translation>
    </message>
    <message>
        <source>An absolute path excluding the file name: c:/foo/bar.rol.zed =&gt; c:/foo</source>
        <translation type="vanished">Абсолютный путь без названия файла: c:/foo/bar.rol.zed =&gt; c:/foo</translation>
    </message>
    <message>
        <source>Returns the name of the file, excluding the path</source>
        <translation type="vanished">Возвращает название файла без пути</translation>
    </message>
    <message>
        <source>Returns the full base name of the file without the path. The full base name consists of all characters in the file up to (but not including) the last dot character</source>
        <translation type="vanished">Возвращает полное название файла без расширения</translation>
    </message>
    <message>
        <source>Full base name</source>
        <translation type="vanished">Название без расширения</translation>
    </message>
    <message>
        <source>Returns the full extension of the file. The full extension consists of all characters in the file after (but not including) the first dot character</source>
        <translation type="vanished">Возвращает полное расширение файла</translation>
    </message>
    <message>
        <source>Full extension</source>
        <translation type="vanished">Полное расширение</translation>
    </message>
    <message>
        <source>Returns an absolute path excluding the file name</source>
        <translation type="vanished">Возвращает абсолютный путь без названия файла</translation>
    </message>
    <message>
        <source>Returns the base name of the file without the path. The base name consists of all characters in the file up to (but not including) the first dot character</source>
        <translation type="vanished">Возвращает название файла без расширения</translation>
    </message>
    <message>
        <source>Returns an absolute path including the file name</source>
        <translation type="vanished">Возвращает абсолютный путь до файла</translation>
    </message>
    <message>
        <source>Returns the extension of the file. The extension consists of all characters in the file after (but not including) the last dot character</source>
        <translation type="vanished">Возвращает расширение файла</translation>
    </message>
    <message>
        <source>Simple counter</source>
        <translation type="vanished">Простой счётчик</translation>
    </message>
    <message>
        <source>Simultaneous conversions:</source>
        <translation type="vanished">Одновременных конвертаций:</translation>
    </message>
    <message>
        <source>Simultaneous processes:</source>
        <translation type="vanished">Процессов одновременно:</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="138"/>
        <source>Load profile...</source>
        <translation>Загрузить профиль...</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="118"/>
        <source>Save profile...</source>
        <translation>Сохранить профиль...</translation>
    </message>
    <message>
        <source>FLAC...</source>
        <translation type="vanished">FLAC...</translation>
    </message>
    <message>
        <source>Simultaneous Conversions:</source>
        <translation type="vanished">Одновременных конвертаций:</translation>
    </message>
    <message>
        <source>How many files can be converted simultaneously. It&apos;s generally recommended to use the number of processors on the local machine. For example, specify 4 on a 4-core machine.</source>
        <translation type="vanished">Количество одновременно запущенных конвертаций. Рекомендуется использовать то же число, что и количество ядер в процессоре. Например, если у Вас процессор с 4-мя ядрами, то укажите 4.</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="70"/>
        <source>Autodetect</source>
        <translation>Автоопределение</translation>
    </message>
    <message>
        <source>Find...</source>
        <translation type="vanished">Найти...</translation>
    </message>
    <message>
        <source>Profiles</source>
        <translation type="vanished">Профили</translation>
    </message>
    <message>
        <source>Load...</source>
        <translation type="vanished">Загрузить...</translation>
    </message>
    <message>
        <source>Save...</source>
        <translation type="vanished">Сохранить...</translation>
    </message>
    <message>
        <source>FLAC</source>
        <translation type="obsolete">FLAC</translation>
    </message>
    <message>
        <source>How many files can be converted simultaneously</source>
        <translation type="vanished">Количество одновременно запущенных конвертаций</translation>
    </message>
    <message>
        <source>Simultaneous Conversions: </source>
        <translation type="vanished">Одновременных конвертаций: </translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="97"/>
        <source>Program:</source>
        <translation>Программа:</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="94"/>
        <location filename="../../forms/configuredialog.ui" line="107"/>
        <source>Path to the conversion program</source>
        <translation>Пусть к программе</translation>
    </message>
    <message>
        <source>Find</source>
        <translation type="vanished">Найти</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Открыть</translation>
    </message>
    <message>
        <source>Arguments:</source>
        <translation type="vanished">Аргументы:</translation>
    </message>
    <message>
        <source>Arguments of the conversion program</source>
        <translation type="vanished">Аргументы программы</translation>
    </message>
    <message>
        <source>Presets:</source>
        <translation type="vanished">Настройки:</translation>
    </message>
    <message>
        <source>Load</source>
        <translation type="vanished">Загрузить</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Сохранить</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="162"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="169"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="189"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../forms/configuredialog.ui" line="196"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <source>with support for Ogg FLAC</source>
        <translation type="obsolete">с поддержкой Ogg FLAC</translation>
    </message>
    <message>
        <source>without support for Ogg FLAC</source>
        <translation type="obsolete">без поддержки Ogg FLAC</translation>
    </message>
</context>
<context>
    <name>FileList</name>
    <message>
        <source>File</source>
        <translation type="obsolete">Файл</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation type="obsolete">Директория</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Размер</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Дата</translation>
    </message>
</context>
<context>
    <name>FileListModel</name>
    <message>
        <source>Ready</source>
        <translation type="obsolete">Готов</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Файл</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation type="obsolete">Директория</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Размер</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Дата</translation>
    </message>
</context>
<context>
    <name>FlacDialog</name>
    <message>
        <source>FLAC</source>
        <translation type="vanished">FLAC</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Отмена</translation>
    </message>
</context>
<context>
    <name>HistoryDialog</name>
    <message>
        <location filename="../../forms/historydialog.ui" line="14"/>
        <source>History</source>
        <translation>История</translation>
    </message>
    <message>
        <location filename="../../forms/historydialog.ui" line="42"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <source>Yanis V. Kurganov</source>
        <comment>Author&apos;s name</comment>
        <translation type="obsolete">Янис В. Курганов</translation>
    </message>
    <message>
        <source>Russia</source>
        <comment>Author&apos;s country</comment>
        <translation type="obsolete">Россия</translation>
    </message>
    <message>
        <source>%1.%2.%3</source>
        <comment>FLAC API version</comment>
        <translation type="obsolete">%1.%2.%3</translation>
    </message>
    <message>
        <source>%1.%2.%3</source>
        <comment>FLAC++ API version</comment>
        <translation type="obsolete">%1.%2.%3</translation>
    </message>
    <message>
        <source>Yanis V. Kurganov</source>
        <comment>Author name</comment>
        <translation type="obsolete">Янис В. Курганов</translation>
    </message>
    <message>
        <source>Russia</source>
        <comment>Author country</comment>
        <translation type="obsolete">Россия</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Program log</source>
        <translation type="obsolete">Лог программы</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Файл</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation type="obsolete">Папка</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Размер</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="135"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="150"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="143"/>
        <location filename="../../forms/mainwindow.ui" line="214"/>
        <location filename="../../forms/mainwindow.ui" line="478"/>
        <source>&amp;Actions</source>
        <translation>&amp;Действия</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="56"/>
        <location filename="../../forms/mainwindow.ui" line="59"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="64"/>
        <location filename="../../forms/mainwindow.ui" line="67"/>
        <source>Directory</source>
        <translation>Директория</translation>
    </message>
    <message>
        <source>Modified</source>
        <translation type="vanished">Изменён</translation>
    </message>
    <message>
        <source>Progress</source>
        <translation type="vanished">Выполнено</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="72"/>
        <location filename="../../forms/mainwindow.ui" line="75"/>
        <source>Status</source>
        <translation>Состояние</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="126"/>
        <location filename="../../forms/mainwindow.ui" line="190"/>
        <location filename="../../forms/mainwindow.ui" line="461"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="93"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="97"/>
        <source>&amp;Language</source>
        <translation>&amp;Язык</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Правка</translation>
    </message>
    <message>
        <source>Actions</source>
        <translation type="obsolete">Действия</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Помощь</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="238"/>
        <source>E&amp;xit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="241"/>
        <location filename="../../forms/mainwindow.ui" line="244"/>
        <source>Exit the application</source>
        <translation>Выйти из программы</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="247"/>
        <source>Ctrl+Q</source>
        <comment>Quit</comment>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <source>Program &amp;log...</source>
        <translation type="obsolete">&amp;Лог программы...</translation>
    </message>
    <message>
        <source>View the program log</source>
        <translation type="obsolete">Показать лог программы</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="vanished">&amp;О программе</translation>
    </message>
    <message>
        <source>View information about the application</source>
        <translation type="obsolete">Показать информацию о программе</translation>
    </message>
    <message>
        <source>About &amp;FLAC</source>
        <translation type="vanished">О &amp;FLAC</translation>
    </message>
    <message>
        <source>View information about FLAC</source>
        <translation type="obsolete">Показать информацию о библиотеке FLAC</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation type="vanished">О &amp;Qt</translation>
    </message>
    <message>
        <source>View information about Qt</source>
        <translation type="obsolete">Показать информацию о библиотеке Qt</translation>
    </message>
    <message>
        <source>&amp;Add files</source>
        <translation type="obsolete">Добавить &amp;файлы</translation>
    </message>
    <message>
        <source>Add files</source>
        <translation type="vanished">Добавить файлы</translation>
    </message>
    <message>
        <source>Add files to the list</source>
        <translation type="vanished">Добавить файлы в список</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="304"/>
        <source>&amp;Remove</source>
        <translation>&amp;Удалить</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Удалить</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="307"/>
        <location filename="../../forms/mainwindow.ui" line="310"/>
        <source>Remove selected files from the list</source>
        <translation>Удалить выделенные файлы из списка</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="325"/>
        <source>&amp;Clear</source>
        <translation>&amp;Очистить</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Очистить</translation>
    </message>
    <message>
        <source>Clear the list</source>
        <translation type="obsolete">Очистить список</translation>
    </message>
    <message>
        <source>Add &amp;folder</source>
        <translation type="obsolete">Добавить &amp;папку</translation>
    </message>
    <message>
        <source>Add folder to the list</source>
        <translation type="vanished">Добавить папку в список</translation>
    </message>
    <message>
        <source>Change language to &apos;%1&apos;</source>
        <translation type="obsolete">Изменить язык на &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Ready</source>
        <comment>Default message of the status bar</comment>
        <translation type="obsolete">Готов</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="106"/>
        <source>&amp;Toolbar</source>
        <translation>Панель &amp;инструментов</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="110"/>
        <source>&amp;Display</source>
        <translation>&amp;Показать</translation>
    </message>
    <message>
        <source>&amp;Options</source>
        <translation type="vanished">&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="157"/>
        <source>&amp;Tools</source>
        <translation>&amp;Инструменты</translation>
    </message>
    <message>
        <source>Remove selected files</source>
        <translation type="vanished">Удалить выбранные файлы</translation>
    </message>
    <message>
        <source>Remove all files</source>
        <translation type="vanished">Удалить все файлы</translation>
    </message>
    <message>
        <source>Add directory</source>
        <translation type="vanished">Добавить директорию</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="259"/>
        <location filename="../../forms/mainwindow.ui" line="262"/>
        <source>Show information about program</source>
        <translation>Показать информацию о программе</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="256"/>
        <source>&amp;About...</source>
        <translation>&amp;О программе...</translation>
    </message>
    <message>
        <source>Source Directory</source>
        <translation type="vanished">Исходная директория</translation>
    </message>
    <message>
        <source>Target Directory</source>
        <translation type="vanished">Целевая директория</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="271"/>
        <source>About &amp;Qt...</source>
        <translation>О &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="286"/>
        <source>Add &amp;files...</source>
        <translation>Добавить &amp;файлы...</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="343"/>
        <source>Add &amp;directory...</source>
        <translation>Добавить &amp;директорию...</translation>
    </message>
    <message>
        <source>Add directory you wish to convert in the list</source>
        <translation type="vanished">Добавить в список все файлы из директории, которую вы хотите конвертировать</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="357"/>
        <source>&amp;Icon Only</source>
        <translation>Только &amp;значок</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="360"/>
        <location filename="../../forms/mainwindow.ui" line="363"/>
        <source>Only display the icon</source>
        <translation>Показать только значок</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="371"/>
        <source>&amp;Text Only</source>
        <translation>Только &amp;название</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="374"/>
        <location filename="../../forms/mainwindow.ui" line="377"/>
        <source>Only display the text</source>
        <translation>Показать только название</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="385"/>
        <source>Text &amp;Beside Icon</source>
        <translation>Название &amp;около значка</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="388"/>
        <location filename="../../forms/mainwindow.ui" line="391"/>
        <source>The text appears beside the icon</source>
        <translation>Название появится около значка</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="402"/>
        <source>Text &amp;Under Icon</source>
        <translation>Название &amp;под значком</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="405"/>
        <location filename="../../forms/mainwindow.ui" line="408"/>
        <source>The text appears under the icon</source>
        <translation>Название появится под значком</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="420"/>
        <source>S&amp;tart converting</source>
        <translation>&amp;Начать конвертирование</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="441"/>
        <source>St&amp;op converting</source>
        <translation>&amp;Остановить конвертирование</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="509"/>
        <source>Select &amp;none</source>
        <translation>&amp;Сбросить выделение</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="423"/>
        <location filename="../../forms/mainwindow.ui" line="426"/>
        <source>Start the conversion of all selected files in the list</source>
        <translation>Начать конвертирование выделенных файлов в списке</translation>
    </message>
    <message>
        <source>Source Format</source>
        <translation type="vanished">Исходный формат</translation>
    </message>
    <message>
        <source>Target Format</source>
        <translation type="vanished">Целевой формат</translation>
    </message>
    <message>
        <source>Output Directory</source>
        <translation type="vanished">Целевая директория</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="289"/>
        <location filename="../../forms/mainwindow.ui" line="292"/>
        <source>Add files you wish to convert to the list</source>
        <translation>Добавить в список файлы, которые вы хотите конвертировать</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="346"/>
        <location filename="../../forms/mainwindow.ui" line="349"/>
        <source>Add all files from directory you wish to convert to the list</source>
        <translation>Добавить в список все файлы из директории, которую вы хотите конвертировать</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="429"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="444"/>
        <location filename="../../forms/mainwindow.ui" line="447"/>
        <source>Stop the conversion of all selected files in the list</source>
        <translation>Остановить конвертирование выделенных файлов в списке</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="450"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="492"/>
        <source>Select &amp;all</source>
        <translation>Выделить &amp;всё</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="495"/>
        <location filename="../../forms/mainwindow.ui" line="498"/>
        <source>Select all files in the list</source>
        <translation>Выделить все файлы в списке</translation>
    </message>
    <message>
        <source>S&amp;tart</source>
        <translation type="vanished">&amp;Запустить</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Запустить</translation>
    </message>
    <message>
        <source>Start conversion selected files in the list</source>
        <translation type="vanished">Начать конвертацию выделенных файлов</translation>
    </message>
    <message>
        <source>Stop conversion selected files in the list</source>
        <translation type="vanished">Остановить конвертацию выделенных файлов</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="527"/>
        <location filename="../../forms/mainwindow.ui" line="530"/>
        <source>Show options</source>
        <translation>Показать настройки</translation>
    </message>
    <message>
        <source>Translate</source>
        <translation type="vanished">Перевести</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="550"/>
        <source>&amp;History...</source>
        <translation>&amp;История...</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="553"/>
        <location filename="../../forms/mainwindow.ui" line="556"/>
        <source>Show program log</source>
        <translation>Показать лог программы</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="559"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <source>Start/continue compressing/decompressing selected files in the list</source>
        <translation type="vanished">Начать конвертацию выделенных файлов</translation>
    </message>
    <message>
        <source>Ctrl+T</source>
        <comment>Start/continue compressing/decompressing</comment>
        <translation type="vanished">Ctrl+T</translation>
    </message>
    <message>
        <source>St&amp;op</source>
        <translation type="vanished">&amp;Остановить</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Остановить</translation>
    </message>
    <message>
        <source>Stop compressing/decompressing selected files in the list</source>
        <translation type="vanished">Остановить процесс конвертации для выделенных файлов</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <comment>Stop compressing/decompressing</comment>
        <translation type="vanished">Ctrl+O</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">Выделить &amp;всё</translation>
    </message>
    <message>
        <source>Select all files</source>
        <translation type="vanished">Выделить все файлы</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="501"/>
        <source>Ctrl+A</source>
        <comment>Select All</comment>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <source>Select &amp;None</source>
        <translation type="vanished">&amp;Сбросить выделение</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="512"/>
        <location filename="../../forms/mainwindow.ui" line="515"/>
        <source>Clear selection</source>
        <translation>Сбросить выделение</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="524"/>
        <source>&amp;Options...</source>
        <translation>&amp;Настройки...</translation>
    </message>
    <message>
        <source>&amp;Configure...</source>
        <translation type="vanished">&amp;Настроить...</translation>
    </message>
    <message>
        <source>Stub</source>
        <comment>Don&apos;t translate</comment>
        <translation type="vanished">Stub</translation>
    </message>
    <message>
        <source>Show the program log</source>
        <translation type="obsolete">Показать лог программы</translation>
    </message>
    <message>
        <source>Show information about the application</source>
        <translation type="vanished">Показать информацию о программе</translation>
    </message>
    <message>
        <source>Show information about FLAC</source>
        <translation type="vanished">Показать информацию о FLAC</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="274"/>
        <location filename="../../forms/mainwindow.ui" line="277"/>
        <source>Show information about Qt</source>
        <translation>Показать информацию о Qt</translation>
    </message>
    <message>
        <source>Add &amp;Files</source>
        <translation type="vanished">Добавить &amp;файлы</translation>
    </message>
    <message>
        <source>Add files you wish to convert in the list</source>
        <translation type="vanished">Добавить в список файлы, которые вы хотите конвертировать</translation>
    </message>
    <message>
        <source>Add &amp;Directory</source>
        <translation type="vanished">Добавить &amp;директорию</translation>
    </message>
    <message>
        <source>Add Files</source>
        <translation type="vanished">Добавить файлы</translation>
    </message>
    <message>
        <source>Add Directory</source>
        <translation type="vanished">Добавить директорию</translation>
    </message>
    <message>
        <source>Can&apos;t install language &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно установить язык &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="313"/>
        <source>Del</source>
        <comment>Remove selected files from the list</comment>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="334"/>
        <source>Ctrl+Del</source>
        <comment>Remove all entries from the list</comment>
        <translation>Ctrl+Del</translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation type="vanished">&amp;Стиль</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="328"/>
        <location filename="../../forms/mainwindow.ui" line="331"/>
        <source>Remove all files from the list</source>
        <translation>Удалить все файлы из списка</translation>
    </message>
    <message>
        <source>&amp;Compress</source>
        <translation type="obsolete">&amp;Запаковать</translation>
    </message>
    <message>
        <source>Start compressing the selected files in the list</source>
        <translation type="obsolete">Запаковать выбранные файлы в списке</translation>
    </message>
    <message>
        <source>&amp;Decompress</source>
        <translation type="obsolete">&amp;Распаковать</translation>
    </message>
    <message>
        <source>Start decompressing the selected files in the list</source>
        <translation type="obsolete">Распаковать выбранные файлы в списке</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="464"/>
        <location filename="../../forms/mainwindow.ui" line="467"/>
        <source>Show or hide &apos;Edit&apos; toolbar</source>
        <translation>Показать или скрыть панель инструментов &apos;Правка&apos;</translation>
    </message>
    <message>
        <location filename="../../forms/mainwindow.ui" line="481"/>
        <location filename="../../forms/mainwindow.ui" line="484"/>
        <source>Show or hide &apos;Actions&apos; toolbar</source>
        <translation>Показать или скрыть панель инструментов &apos;Действия&apos;</translation>
    </message>
    <message>
        <source>Show or hide &apos;Help&apos; toolbar</source>
        <translation type="vanished">Показать или скрыть панель инструментов &apos;Справка&apos;</translation>
    </message>
    <message>
        <source>Change style to &apos;%1&apos;</source>
        <translation type="obsolete">Изменить стиль на &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t install style &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно установить стиль &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>MediaConverter::ConfigureDialog</name>
    <message>
        <source>Find program</source>
        <translation type="vanished">Найти программу</translation>
    </message>
    <message>
        <location filename="../../sources/configuredialog.cpp" line="78"/>
        <source>Load profile</source>
        <translation>Загрузить профиль</translation>
    </message>
    <message>
        <location filename="../../sources/configuredialog.cpp" line="84"/>
        <source>Wrong profile format</source>
        <translation>Неверный формат профиля</translation>
    </message>
    <message>
        <location filename="../../sources/configuredialog.cpp" line="104"/>
        <source>Save profile</source>
        <translation>Сохранить профиль</translation>
    </message>
</context>
<context>
    <name>MediaConverter::FileItem</name>
    <message>
        <location filename="../../sources/fileitem.cpp" line="91"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemstate.cpp" line="41"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemstate.cpp" line="59"/>
        <source>Queued</source>
        <translation>В очереди</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemstate.cpp" line="93"/>
        <source>Finished</source>
        <translation>Завершено</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemstate.cpp" line="110"/>
        <source>Stopped</source>
        <translation>Остановлено</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemstate.cpp" line="127"/>
        <source>Completed</source>
        <translation>Завершено</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemstate.cpp" line="144"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>MediaConverter::FileItemCreator</name>
    <message>
        <location filename="../../sources/fileitemcreator.cpp" line="83"/>
        <source>%1&lt;br&gt;%2 files processed</source>
        <translation>%1&lt;br&gt;%2 файлов обработано</translation>
    </message>
</context>
<context>
    <name>MediaConverter::FileItemList</name>
    <message>
        <location filename="../../sources/fileitemlist.cpp" line="107"/>
        <source>Adding files...</source>
        <translation>Добавление файлов...</translation>
    </message>
    <message>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to remove?</source>
        <translation type="vanished">Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите удалить выделенные файлы из списка?</translation>
    </message>
    <message>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to clear?</source>
        <translation type="vanished">Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите удалить все файлы из списка?</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemlist.cpp" line="156"/>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to remove selected files from the list?</source>
        <translation>Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите удалить выделенные файлы из списка?</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemlist.cpp" line="164"/>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to remove all files from the list?</source>
        <translation>Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите удалить все файлы из списка?</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemlist.cpp" line="214"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../../sources/fileitemlist.cpp" line="214"/>
        <source>Are you sure you want to add %1 files?&lt;br&gt;This may decrease performance considerably.</source>
        <translation>Вы действительно хотите добавить %1 файлов?&lt;br&gt;Производительность программы при этом может сильно пострадать.</translation>
    </message>
</context>
<context>
    <name>MediaConverter::MainWindow</name>
    <message>
        <location filename="../../sources/mainwindow.cpp" line="91"/>
        <source>Can&apos;t find language &apos;%1&apos;</source>
        <translation>Невозможно найти  язык &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../sources/mainwindow.cpp" line="113"/>
        <source>Change language to &apos;%1&apos;</source>
        <translation>Изменить язык на &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../sources/mainwindow.cpp" line="136"/>
        <source>Can&apos;t install language &apos;%1&apos;</source>
        <translation>Невозможно установить язык &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../sources/mainwindow.cpp" line="145"/>
        <source>Add files</source>
        <translation>Добавить файлы</translation>
    </message>
    <message>
        <location filename="../../sources/mainwindow.cpp" line="154"/>
        <source>Add directory</source>
        <translation>Добавить директорию</translation>
    </message>
    <message>
        <location filename="../../sources/mainwindow.cpp" line="171"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>%1, version %2, architecture %3&lt;br&gt;Copyright (C) %4 %5 &amp;lt;&lt;a href=mailto:%6?subject&amp;#61;%1&gt;%6&lt;/a&gt;&amp;gt;&lt;br&gt;&lt;a href=%7&gt;%7&lt;/a&gt;</source>
        <translation type="vanished">%1, версия %2, архитектура %3&lt;br&gt;Авторское право (C) %4 %5 &amp;lt;&lt;a href=mailto:%6?subject&amp;#61;%1&gt;%6&lt;/a&gt;&amp;gt;&lt;br&gt;&lt;a href=%7&gt;%7&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../sources/mainwindow.cpp" line="172"/>
        <source>%1, version %2, architecture %3&lt;br&gt;Copyright (C) %4 %5 &amp;lt;&lt;a href=mailto:%6?subject&amp;#61;%7&gt;%6&lt;/a&gt;&amp;gt;&lt;br&gt;&lt;a href=%8&gt;%8&lt;/a&gt;</source>
        <translation>%1, версия %2, архитектура %3&lt;br&gt;Авторское право (C) %4 %5 &amp;lt;&lt;a href=mailto:%6?subject&amp;#61;%7&gt;%6&lt;/a&gt;&amp;gt;&lt;br&gt;&lt;a href=%8&gt;%8&lt;/a&gt;</translation>
    </message>
    <message>
        <source>%1, version %2, architecture %3&lt;br&gt;Copyright (C) %4 %5 &amp;lt;&lt;a href=mailto:%6?subject=%7&gt;%6&lt;/a&gt;&amp;gt;&lt;br&gt;&lt;a href=%8&gt;%8&lt;/a&gt;</source>
        <translation type="vanished">%1, версия %2, архитектура %3&lt;br&gt;Авторское право (C) %4 %5 &amp;lt;&lt;a href=mailto:%6?subject=%7&gt;%6&lt;/a&gt;&amp;gt;&lt;br&gt;&lt;a href=%8&gt;%8&lt;/a&gt;</translation>
    </message>
    <message>
        <source>%1, version %2, architecture %3&lt;br&gt;Copyright (C) %4 %5 &amp;lt;%6&amp;gt;&lt;br&gt;&lt;a href=%7&gt;%7&lt;/a&gt;</source>
        <translation type="vanished">%1, версия %2, архитектура %3&lt;br&gt;Авторское право (C) %4 %5 &amp;lt;%6&amp;gt;&lt;br&gt;&lt;a href=%7&gt;%7&lt;/a&gt;</translation>
    </message>
    <message>
        <source>%1, version %2, architecture %3&lt;br&gt;Copyright (C) %4 %5 &lt;%6&gt;&lt;br&gt;&lt;a href=%7&gt;%7&lt;/a&gt;</source>
        <translation type="vanished">%1, версия %2, архитектура %3&lt;br&gt;Авторское право (C) %4 %5 &lt;%6&gt;&lt;br&gt;&lt;a href=%7&gt;%7&lt;/a&gt;</translation>
    </message>
    <message>
        <source>%1, version %2, architecture %3&lt;br&gt;Copyright (C) %4 %5&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;</source>
        <translation type="vanished">%1, версия %2, архитектура %3&lt;br&gt;Авторское право (C) %4 %5&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;</translation>
    </message>
    <message>
        <source>%1, version %2, architecture %3&lt;br&gt;Copyright (C) %4 %5&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;&lt;br&gt;&lt;a href=%7&gt;%7&lt;/a&gt;&lt;br&gt;</source>
        <translation type="vanished">%1, версия %2, архитектура %3&lt;br&gt;Авторское право (C) %4 %5&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;&lt;br&gt;&lt;a href=%7&gt;%7&lt;/a&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>%1, version %2&lt;br&gt;Copyright (C) %3 %4&lt;br&gt;&lt;a href=%5&gt;%5&lt;/a&gt;&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;&lt;br&gt;</source>
        <translation type="vanished">%1, версия %2&lt;br&gt;Авторское право (C) %3 %4&lt;br&gt;&lt;a href=%5&gt;%5&lt;/a&gt;&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>%1 %2&lt;br&gt;Copyright (C) %3 %4&lt;br&gt;&lt;a href=%5&gt;%5&lt;/a&gt;&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;&lt;br&gt;</source>
        <translation type="vanished">%1 %2&lt;br&gt;Авторское право (C) %3 %4&lt;br&gt;&lt;a href=%5&gt;%5&lt;/a&gt;&lt;br&gt;&lt;a href=%6&gt;%6&lt;/a&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../../sources/mainwindow.cpp" line="188"/>
        <source>There are unfinished conversions.&lt;br&gt;Are you sure you want to exit?</source>
        <translation>Некоторые файлы всё ещё конвертируются.&lt;br&gt;Вы действительно хотите выйти?</translation>
    </message>
</context>
<context>
    <name>MediaConverter::SettingsManager</name>
    <message>
        <source>Unsupported Qt version</source>
        <translation type="obsolete">Неподдерживаемая версия Qt</translation>
    </message>
</context>
<context>
    <name>MediaConverter::StatusBar</name>
    <message>
        <location filename="../../sources/statusbar.cpp" line="33"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
</context>
<context>
    <name>MediaConverter::Task</name>
    <message>
        <source>The program failed to start. Either the invoked program is missing, or you may have insufficient permissions to invoke the program</source>
        <translation type="vanished">Невозможно запустить программу. Укажите в настройках программу для конвертации файлов и убедитесь в том, что у Вас есть все необходимые права на её запуск</translation>
    </message>
    <message>
        <source>The program crashed some time after starting successfully</source>
        <translation type="vanished">Программа запустилась успешно, но неожиданно завершила работу аварийно</translation>
    </message>
    <message>
        <source>The program timed out</source>
        <translation type="vanished">Достигнуто ограничение по времени для программы</translation>
    </message>
    <message>
        <source>An error occurred when attempting to write to the program</source>
        <translation type="vanished">Невозможно отправить данные в программу</translation>
    </message>
    <message>
        <source>An error occurred when attempting to read from the program</source>
        <translation type="vanished">Невозможно получить данные из программы</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation type="vanished">Произошла неизвестная ошибка</translation>
    </message>
    <message>
        <source>Unsupported Qt version</source>
        <translation type="vanished">Неподдерживаемая версия Qt</translation>
    </message>
    <message>
        <source>The program exited normally, but return code is %1</source>
        <translation type="vanished">Программа завершилась успешно, но код ответа %1</translation>
    </message>
    <message>
        <source>The program crashed</source>
        <translation type="vanished">Программа завершилась аварийно</translation>
    </message>
</context>
<context>
    <name>MediaConverter::Translation</name>
    <message>
        <source>Can&apos;t load translator</source>
        <translation type="vanished">Невозможно загрузить переводчик</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="51"/>
        <source>Language name doesn&apos;t conform to ISO639_ISO3166</source>
        <translation>Название языка не соответствует стандарту ISO639_ISO3166</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="89"/>
        <source>Can&apos;t load default language: &apos;%1&apos;</source>
        <translation>Невозможно загрузить язык по умолчанию: &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t find &apos;%1&apos;</source>
        <translation type="vanished">Невозможно найти &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t access language directory &apos;%1&apos;: no such directory or not readable</source>
        <translation type="vanished">Невозможно получить доступ к директории с языками &apos;%1&apos;: нет такой директории или она нечитаема</translation>
    </message>
    <message>
        <source>Can&apos;t load translation file</source>
        <translation type="vanished">Невозможно загрузить файл с переводом</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="54"/>
        <source>Can&apos;t load language flag</source>
        <translation>Невозможно загрузить флаг языка</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="100"/>
        <source>Can&apos;t find archive entry &apos;%1&apos;</source>
        <translation>Невозможно найти элемент &apos;%1&apos; в архиве</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="123"/>
        <source>Language directory &apos;%1&apos; doesn&apos;t exist or not readable</source>
        <translation>Директория с языковыми файлами &apos;%1&apos; не существует или нечитаема</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="117"/>
        <source>Loading language &apos;%1&apos;... ok</source>
        <translation>Загружается язык &apos;%1&apos;... успешно</translation>
    </message>
    <message>
        <source>Language directory is &apos;%1&apos;</source>
        <translation type="vanished">Директория с языковыми файлами &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="45"/>
        <source>Can&apos;t load qt translation file</source>
        <translation>Невозможно загрузить файл с переводом qt</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="48"/>
        <source>Can&apos;t load application translation file</source>
        <translation>Невозможно загрузить файл с переводом приложения</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="119"/>
        <source>Loading language &apos;%1&apos;... failed: &apos;%2&apos;</source>
        <translation>Загружается язык &apos;%1&apos;... неудачно: &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="133"/>
        <location filename="../../sources/translation.cpp" line="154"/>
        <source>Installing language &apos;%1&apos;... ok</source>
        <translation>Устанавливается язык &apos;%1&apos;... успешно</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="149"/>
        <source>Installing language &apos;%1&apos;... failed</source>
        <translation>Устанавливается язык &apos;%1&apos;... неудачно</translation>
    </message>
    <message>
        <location filename="../../sources/translation.cpp" line="187"/>
        <source>Can&apos;t find language &apos;%1&apos;</source>
        <translation>Невозможно найти  язык &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>QFLAC::Application::Info</name>
    <message>
        <source>Yanis V. Kurganov</source>
        <translation type="obsolete">Янис В. Курганов</translation>
    </message>
    <message>
        <source>Russia</source>
        <translation type="obsolete">Россия</translation>
    </message>
    <message>
        <source>%1.%2.%3</source>
        <comment>FLAC API Version</comment>
        <translation type="obsolete">%1.%2.%3</translation>
    </message>
    <message>
        <source>%1.%2.%3</source>
        <comment>FLAC++ API Version</comment>
        <translation type="obsolete">%1.%2.%3</translation>
    </message>
    <message>
        <source>Yanis V. Kurganov</source>
        <comment>Author Name</comment>
        <translation type="obsolete">Янис В. Курганов</translation>
    </message>
    <message>
        <source>Russia</source>
        <comment>Author Country</comment>
        <translation type="obsolete">Россия</translation>
    </message>
</context>
<context>
    <name>QFLAC::Application::Service</name>
    <message>
        <source>Internal program error</source>
        <translation type="obsolete">Внутренняя ошибка</translation>
    </message>
</context>
<context>
    <name>QFLAC::FileList</name>
    <message>
        <source>File</source>
        <translation type="obsolete">Файл</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation type="obsolete">Директория</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Размер</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Дата</translation>
    </message>
</context>
<context>
    <name>QFLAC::MainWindow</name>
    <message>
        <source>Change language to &apos;%1&apos;</source>
        <translation type="obsolete">Изменить язык на &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Change style to &apos;%1&apos;</source>
        <translation type="obsolete">Изменить стиль на &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t install language &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно установить язык &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t install style &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно установить стиль &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Add Files</source>
        <translation type="obsolete">Добавить файлы</translation>
    </message>
    <message>
        <source>Add Directory</source>
        <translation type="obsolete">Добавить директорию</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>Program log</source>
        <translation type="obsolete">Лог программы</translation>
    </message>
</context>
<context>
    <name>QFLAC::StatusBar</name>
    <message>
        <source>Ready</source>
        <translation type="obsolete">Готов</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::AboutDialog::AboutDialogImpl</name>
    <message>
        <source>About</source>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::AboutFLACDialog::AboutFLACDialogImpl</name>
    <message>
        <source>About FLAC</source>
        <translation type="obsolete">О FLAC</translation>
    </message>
    <message>
        <source>&amp;Features</source>
        <translation type="obsolete">&amp;Возможности</translation>
    </message>
    <message>
        <source>&amp;Goals</source>
        <translation type="obsolete">&amp;Цели</translation>
    </message>
    <message>
        <source>&amp;License</source>
        <translation type="obsolete">&amp;Лицензия</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3, but lossless, meaning that audio is compressed in FLAC without any loss in quality. This is similar to how Zip works, except with FLAC you will get much better compression because it is designed specifically for audio, and you can play back compressed FLAC files in your favorite player (or your car or home stereo) just like you would an MP3 file.&lt;/p&gt;&lt;p&gt;FLAC supports tagging, cover art, and fast seeking. FLAC is freely available and supported on most operating systems, including Windows, &amp;quot;unix&amp;quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2, and Amiga.&lt;/p&gt;&lt;p&gt;When we say that FLAC is &amp;quot;Free&amp;quot; it means more than just that it is available at no cost. It means that the specification of the format is fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance), and that neither the FLAC format nor any of the implemented encoding/decoding methods are covered by any known patent. It also means that all the source code is available under open-source licenses. It is the first truly open and free lossless audio format.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;FLAC - это аббревиатура от Free Lossless Audio Codec (свободный аудиокодек, сжимающий без потерь). Проект FLAC включает:&lt;ul&gt;&lt;li&gt;потоковый формат;&lt;/li&gt;&lt;li&gt;базовые кодеры и декодеры в виде библиотек;&lt;/li&gt;&lt;li&gt;&lt;tt&gt;flac&lt;/tt&gt;, утилиту командной строки, выполняющую сжатие и распаковку файлов .flac;&lt;/li&gt;&lt;li&gt;&lt;tt&gt;metaflac&lt;/tt&gt;, утилиту командной строки для редактирования метаданных в файлах .flac;&lt;/li&gt;&lt;li&gt;плагины для разных плейеров.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;FLAC компилируется на множестве платформ: Windows, &amp;quot;unix&amp;quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2 и Amiga. Имеются системы сборки для autoconf/automake, MSVC, Watcom C и Project Builder.&lt;/p&gt;&lt;p&gt;&amp;quot;Свобода&amp;quot; означает, что спецификация потокового формата открыта для всех и может быть использована для любых целей (проект FLAC оставляет за собой право устанавливать спецификации и сертифицировать относящиеся к нему продукты на совместимость), а также то, что ни формат, ни один из реализованных методов кодирования/декодирования не запатентованы. Это также значит, что все исходные тексты доступны по лицензиям, обязывающим предоставлять исходные коды.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Using</source>
        <translation type="obsolete">Используется</translation>
    </message>
    <message>
        <source>with support for Ogg FLAC</source>
        <translation type="obsolete">с поддержкой Ogg FLAC</translation>
    </message>
    <message>
        <source>without support for Ogg FLAC</source>
        <translation type="obsolete">без поддержки Ogg FLAC</translation>
    </message>
    <message>
        <source>See&lt;br&gt;&lt;a href=&quot;http://flac.sourceforge.net/&quot;&gt;flac.sourceforge.net&lt;/a&gt;&lt;br&gt;for more information.</source>
        <translation type="obsolete">См.&lt;br&gt;&lt;a href=&quot;http://flac.sourceforge.net/&quot;&gt;flac.sourceforge.net&lt;/a&gt;&lt;br&gt;для получения более подробной информации.</translation>
    </message>
    <message>
        <source>FLAC: features</source>
        <translation type="obsolete">FLAC: возможности</translation>
    </message>
    <message>
        <source>&lt;p&gt;Notable features of FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Lossless:&lt;/b&gt; The encoding of audio (PCM) data incurs no loss of information, and the decoded audio is bit-for-bit identical to what went into the encoder. Each frame contains a 16-bit CRC of the frame data for detecting transmission errors. The integrity of the audio data is further insured by storing an MD5 signature of the original unencoded audio data in the file header, which can be compared against later during decoding or testing.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Fast:&lt;/b&gt; FLAC is asymmetric in favor of decode speed. Decoding requires only integer arithmetic, and is much less compute-intensive than for most perceptual codecs. Real-time decode performance is easily achievable on even modest hardware.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Hardware support:&lt;/b&gt; FLAC is supported by dozens of consumer electronic devices, from portable players, to home stereo equipment, to car stereo.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Flexible metadata:&lt;/b&gt; FLAC&apos;s metadata system supports tags, cover art, seek tables, and cue sheets. Applications can write their own APPLICATION metadata once they register an ID. New metadata blocks can be defined and implemented in future versions of FLAC without breaking older streams or decoders.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Seekable:&lt;/b&gt; FLAC supports fast sample-accurate seeking. Not only is this useful for playback, it makes FLAC files suitable for use in editing applications.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Streamable:&lt;/b&gt; Each FLAC frame contains enough data to decode that frame. FLAC does not even rely on previous or following frames. FLAC uses sync codes and CRCs (similar to MPEG and other formats), which, along with framing, allow decoders to pick up in the middle of a stream with a minimum of delay.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Suitable for archiving:&lt;/b&gt; FLAC is an open format, and there is no generation loss if you need to convert your data to another format in the future. In addition to the frame CRCs and MD5 signature, flac has a verify option that decodes the encoded stream in parallel with the encoding process and compares the result to the original, aborting with an error if there is a mismatch.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Convenient CD archiving:&lt;/b&gt; FLAC has a &amp;quot;cue sheet&amp;quot; metadata block for storing a CD table of contents and all track and index points. For instance, you can rip a CD to a single file, then import the CD&apos;s extracted cue sheet while encoding to yield a single file representation of the entire CD. If your original CD is damaged, the cue sheet can be exported later in order to burn an exact copy.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Error resistant:&lt;/b&gt; Because of FLAC&apos;s framing, stream errors limit the damage to the frame in which the error occurred, typically a small fraction of a second worth of data. Contrast this with some other lossless codecs, in which a single error destroys the remainder of the stream.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;What FLAC is &lt;b&gt;not&lt;/b&gt;:&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Характеристики FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Сжатие без потерь:&lt;/b&gt; Кодирование PCM данных не приводит к потере информации, следовательно декодируемый аудиофайл абсолютно идентичен тому, который был подан на вход кодеру. Чтобы определить возможные ошибки при передаче файла, для каждого фрейма вычисляется 16-битная контрольная сумма. Целостность на дальнейшем этапе подтверждается подписью MD5 распакованных данных, которая находится в заголовке и может быть проверена при воспроизведении, декодировании или с помощью тестирования.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Скорость:&lt;/b&gt; Скорость работы при кодировании и декодировании несимметричны. Для декодирования используется только целочисленная арифметика, которая требует значительно меньше вычислений, чем в перцепционных кодеках. Декодирование в реальном времени легко достижимо даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Аппаратная поддержка:&lt;/b&gt; Благодаря свободной базовой реализации и простому декодированию FLAC является единственным аудиокодеком, сжимающим без потерь, который имеет аппаратную поддержку.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Метаданные:&lt;/b&gt; FLAC имеет расширяемую систему метаданных. Новые блоки метаданных могут быть определены и реализованы в будущих версиях без потери обратной совместимости. Сейчас определены типы метаданных для таблиц поиска, тегов и списков разметки аудиодисков. Приложение может использовать блок метаданных &lt;tt&gt;APPLICATION&lt;/tt&gt; после регистрации для него &lt;tt&gt;ID&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поиск:&lt;/b&gt; FLAC поддерживает быстрый и точный поиск, что полезно не только при воспроизведении, но и дает возможность использовать FLAC в звуковых редакторах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поточность:&lt;/b&gt; Каждый фрейм FLAC содержит достаточно информации для собственного декодирования. Текущий фрейм FLAC не зависит от предыдущих и последующих. FLAC использует коды синхронизации и контрольные суммы, что позволяет декодеру быстро выбирать позицию в текущем потоке.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование:&lt;/b&gt; FLAC удобно использовать для архивации, так как сжатие с его помощью не приводит к потере информации. Если в будущем Вы решите использовать другой формат, данные будут восстановлены из .flac файла в первоначальном виде. Кроме контрольной суммы фрейма и подписи MD5, утилита &lt;tt&gt;flac&lt;/tt&gt; имеет возможность проверки, использование которой приводит к тому, что кодируемый поток сразу же декодируется и сравнивается с исходным. Если происходит ошибка, кодер прекращает работу.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование аудиодисков:&lt;/b&gt; У FLAC если блок метаданных &amp;quot;cue sheet&amp;quot;, в котором сохраняется таблица разметки аудиодиска. Например, можно записать аудиодиск в один файл, а затем импортировать его таблицу разметки при кодировании, чтобы полученный файл имел такое же представление как и диск. Если оригинал аудиодиска будет испорчен, то вы сможете восстановить таблицу разметки, чтобы записать точную копию диска.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Устойчивость от ошибок:&lt;/b&gt; Благодаря разбиению на фреймы, ошибки в потоке локализуются до уровня фрейма, в котором произошла ошибка (обычно несколько сотых секунды). В некоторых кодеках одна ошибка может привести к потере всего остатка потока.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;Чего &lt;b&gt;нет&lt;/b&gt; во FLAC:&lt;ul&gt;&lt;li&gt;FLAC не осуществляет сжатие с потерями. Для этого существует много хороших форматов, таких как Ogg Vorbis, MPC и MP3 (отличная реализация с открытими исходными текстами LAME).&lt;/li&gt;&lt;li&gt;FLAC не будет SDMI совместимым и т.п. Перед проектом не стоит цели поддерживать методы защиты, которые на практике лишь увеличивают объем файла. Конечно, мы не сможем препятствовать кому-либо создавать несвободные блоки метаданных, однако, стандартные декодеры все равно будут их пропускать.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>FLAC: goals</source>
        <translation type="obsolete">FLAC: цели</translation>
    </message>
    <message>
        <source>&lt;p&gt;Since FLAC is an open-source project, it&apos;s important to have a set of goals that everyone works to. They may change slightly from time to time but they&apos;re a good guideline. Changes should be in line with the goals and should not attempt to embrace any of the anti-goals.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC should be and stay an open format with an open-source reference implementation.&lt;/li&gt;&lt;li&gt;FLAC should be lossless. This seems obvious but lossy compression seems to creep into every audio codec. This goal also means that flac should stay archival quality and be truly lossless for all input. Testing of releases should be thorough.&lt;/li&gt;&lt;li&gt;FLAC should yield respectable compression, on par or better than other lossless codecs.&lt;/li&gt;&lt;li&gt;FLAC should allow at least realtime decoding on even modest hardware.&lt;/li&gt;&lt;li&gt;FLAC should support fast sample-accurate seeking.&lt;/li&gt;&lt;li&gt;FLAC should allow gapless playback of consecutive streams. This follows from the lossless goal.&lt;/li&gt;&lt;li&gt;The FLAC project owes a lot to the many people who have advanced the audio compression field so freely, and aims also to contribute through the open-source development of new ideas.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Anti-goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Так как FLAC - это открытый проект, важно определить список целей, к чему нужно стремиться. Время от времени они могут немного изменяться, но всегда должны определять направление развития. изменения должны согласовываться с текущими целями и не пытаться включить в себя антицели.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Цели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC должен оставаться открытым форматом. Все исходные тексты либо под LGPL, либо под GPL.&lt;/li&gt;&lt;li&gt;FLAC должен производить только сжатие без потерь. Вроде бы это очевидно, однако, кодирование с потерями пытается проникнуть во все аудио кодеки. Эта цель также означает, что FLAC должен придерживаться только принципов архивирования и сжимать без потерь абсолютно все типы входных данных. Релизы должны тщательно тестироваться.&lt;/li&gt;&lt;li&gt;FLAC должен достичь приемлимого уровня сжатия файлов.&lt;/li&gt;&lt;li&gt;FLAC должен иметь низкие аппаратные требования и обеспечить декодирование в реальном времени даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать быстрый и точный поиск.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать воспроизведение без пауз для следующих друг за другом потоков.&lt;/li&gt;&lt;li&gt;Проект FLAC находится в долгу перед многими людьми, кто улучшал методы сжатия звука, и нацелен на поддержку новых идей с помощью открытой разработки.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Антицели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Сжатие с потерями. Существует достаточно много хороших форматов, предназначенных именно для этого (Ogg Vorbis, MP3, и т.д.).&lt;/li&gt;&lt;li&gt;Защита от копирования в любом виде.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>FLAC: license</source>
        <translation type="obsolete">FLAC: лицензия</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC is a free codec in the fullest sense. This page explicitly states all that you may do with the format and software.&lt;/p&gt;&lt;p&gt;The FLAC and Ogg FLAC formats themselves, and their specifications, are fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance). They are free for commercial or noncommercial use. That means that commercial developers may independently write FLAC or Ogg FLAC software which is compatible with the specifications for no charge and without restrictions of any kind. There are no licensing fees of any kind for use of the formats or their specifications, or for distributing, selling, or streaming media in the FLAC or Ogg FLAC formats.&lt;/p&gt;&lt;p&gt;The FLAC project also makes available software that implements the formats, which is distributed according to Open Source licenses as follows:&lt;/p&gt;&lt;p&gt;The reference implementation libraries are licensed under Xiph&apos;s variant of the BSD License, a.k.a. the Xiph License. This is a trivial variation of the popular BSD License where the Xiph.org Foundation is substituted for the University of California, Berkeley. In simple terms, these libraries may be used by any application, Open or proprietary, linked or incorporated in whole, so long as acknowledgement is made to Xiph.org Foundation when using the source code in whole or in derived works. The Xiph License is free enough that the libraries have been used in commercial products to implement FLAC, including in the firmware of hardware devices where other Open Source licenses can be problematic. In the source code these libraries are called libFLAC and libFLAC++.&lt;/p&gt;&lt;p&gt;The rest of the software that the FLAC project provides is licensed under the GNU General Public License (GPL). This software includes various utilities for converting files to and from FLAC format, plugins for audio players, et cetera. In general, the GPL allows redistribution as long as derived works are also made available in source code form according to compatible terms.&lt;/p&gt;&lt;p&gt;Neither the FLAC nor Ogg FLAC formats nor any of the implemented encoding/decoding methods are covered by any known patent.&lt;/p&gt;&lt;p&gt;FLAC is one of a family of codecs of the Xiph.org Foundation, all created according to the same free ideals. For some other codecs&apos; descriptions of the Xiph License see the Speex and Vorbis license pages.&lt;/p&gt;&lt;p&gt;If you would like to redistribute parts or all of FLAC under different terms, contact &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Josh Coalson.&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;...&lt;/p&gt;&lt;p&gt;FLAC - свободный кодек в самом полном смысле этого слова. Ниже описано то, что Вы можете сделать с форматом и программным обеспечением.&lt;/p&gt;&lt;p&gt;FLAC и Ogg FLAC форматы непосредственно, и их спецификации, являются полностью открытыми, могут использоваться для любой цели (проект FLAC оставляет за собой право установить спецификацию FLAC). Они бесплатные для коммерческого или некоммерческого использования. Это означает, что коммерческие разработчики могут независимо написать FLAC или Ogg FLAC программное обеспечение, которое является совместимым со спецификациями без ограничений любого вида. Нет никаких лицензионных сборов для использования форматов или их спецификаций, для распространения, продажи.&lt;/p&gt;&lt;p&gt;Ни FLAC, ни Ogg FLAC форматы, ни любой из осуществленных методов кодирования/расшифровки не закрыты никаким известным патентом.&lt;/p&gt;&lt;p&gt;Если Вы хотели бы распространить части или весь FLAC согласно другим условиям, свяжитесь с &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Джошем Коалсоном&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;...&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Features</source>
        <translation type="obsolete">Возможности</translation>
    </message>
    <message>
        <source>Goals</source>
        <translation type="obsolete">Цели</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="obsolete">Лицензия</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3, but lossless, meaning that audio is compressed in FLAC without any loss in quality. This is similar to how Zip works, except with FLAC you will get much better compression because it is designed specifically for audio, and you can play back compressed FLAC files in your favorite player (or your car or home stereo) just like you would an MP3 file.&lt;/p&gt;&lt;p&gt;FLAC supports tagging, cover art, and fast seeking. FLAC is freely available and supported on most operating systems, including Windows, &quot;unix&quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2, and Amiga.&lt;/p&gt;&lt;p&gt;When we say that FLAC is &quot;Free&quot; it means more than just that it is available at no cost. It means that the specification of the format is fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance), and that neither the FLAC format nor any of the implemented encoding/decoding methods are covered by any known patent. It also means that all the source code is available under open-source licenses. It is the first truly open and free lossless audio format.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;FLAC - это аббревиатура от Free Lossless Audio Codec (свободный аудиокодек, сжимающий без потерь). Проект FLAC включает:&lt;ul&gt;&lt;li&gt;потоковый формат;&lt;/li&gt;&lt;li&gt;базовые кодеры и декодеры в виде библиотек;&lt;/li&gt;&lt;li&gt;&lt;tt&gt;flac&lt;/tt&gt;, утилиту командной строки, выполняющую сжатие и распаковку файлов .flac;&lt;/li&gt;&lt;li&gt;&lt;tt&gt;metaflac&lt;/tt&gt;, утилиту командной строки для редактирования метаданных в файлах .flac;&lt;/li&gt;&lt;li&gt;плагины для разных плейеров.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;FLAC компилируется на множестве платформ: Windows, &quot;unix&quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2 и Amiga. Имеются системы сборки для autoconf/automake, MSVC, Watcom C и Project Builder.&lt;/p&gt;&lt;p&gt;&quot;Свобода&quot; означает, что спецификация потокового формата открыта для всех и может быть использована для любых целей (проект FLAC оставляет за собой право устанавливать спецификации и сертифицировать относящиеся к нему продукты на совместимость), а также то, что ни формат, ни один из реализованных методов кодирования/декодирования не запатентованы. Это также значит, что все исходные тексты доступны по лицензиям, обязывающим предоставлять исходные коды.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Notable features of FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Lossless:&lt;/b&gt; The encoding of audio (PCM) data incurs no loss of information, and the decoded audio is bit-for-bit identical to what went into the encoder. Each frame contains a 16-bit CRC of the frame data for detecting transmission errors. The integrity of the audio data is further insured by storing an MD5 signature of the original unencoded audio data in the file header, which can be compared against later during decoding or testing.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Fast:&lt;/b&gt; FLAC is asymmetric in favor of decode speed. Decoding requires only integer arithmetic, and is much less compute-intensive than for most perceptual codecs. Real-time decode performance is easily achievable on even modest hardware.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Hardware support:&lt;/b&gt; FLAC is supported by dozens of consumer electronic devices, from portable players, to home stereo equipment, to car stereo.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Flexible metadata:&lt;/b&gt; FLAC&apos;s metadata system supports tags, cover art, seek tables, and cue sheets. Applications can write their own APPLICATION metadata once they register an ID. New metadata blocks can be defined and implemented in future versions of FLAC without breaking older streams or decoders.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Seekable:&lt;/b&gt; FLAC supports fast sample-accurate seeking. Not only is this useful for playback, it makes FLAC files suitable for use in editing applications.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Streamable:&lt;/b&gt; Each FLAC frame contains enough data to decode that frame. FLAC does not even rely on previous or following frames. FLAC uses sync codes and CRCs (similar to MPEG and other formats), which, along with framing, allow decoders to pick up in the middle of a stream with a minimum of delay.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Suitable for archiving:&lt;/b&gt; FLAC is an open format, and there is no generation loss if you need to convert your data to another format in the future. In addition to the frame CRCs and MD5 signature, flac has a verify option that decodes the encoded stream in parallel with the encoding process and compares the result to the original, aborting with an error if there is a mismatch.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Convenient CD archiving:&lt;/b&gt; FLAC has a &quot;cue sheet&quot; metadata block for storing a CD table of contents and all track and index points. For instance, you can rip a CD to a single file, then import the CD&apos;s extracted cue sheet while encoding to yield a single file representation of the entire CD. If your original CD is damaged, the cue sheet can be exported later in order to burn an exact copy.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Error resistant:&lt;/b&gt; Because of FLAC&apos;s framing, stream errors limit the damage to the frame in which the error occurred, typically a small fraction of a second worth of data. Contrast this with some other lossless codecs, in which a single error destroys the remainder of the stream.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;What FLAC is &lt;b&gt;not&lt;/b&gt;:&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Характеристики FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Сжатие без потерь:&lt;/b&gt; Кодирование PCM данных не приводит к потере информации, следовательно декодируемый аудиофайл абсолютно идентичен тому, который был подан на вход кодеру. Чтобы определить возможные ошибки при передаче файла, для каждого фрейма вычисляется 16-битная контрольная сумма. Целостность на дальнейшем этапе подтверждается подписью MD5 распакованных данных, которая находится в заголовке и может быть проверена при воспроизведении, декодировании или с помощью тестирования.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Скорость:&lt;/b&gt; Скорость работы при кодировании и декодировании несимметричны. Для декодирования используется только целочисленная арифметика, которая требует значительно меньше вычислений, чем в перцепционных кодеках. Декодирование в реальном времени легко достижимо даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Аппаратная поддержка:&lt;/b&gt; Благодаря свободной базовой реализации и простому декодированию FLAC является единственным аудиокодеком, сжимающим без потерь, который имеет аппаратную поддержку.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Метаданные:&lt;/b&gt; FLAC имеет расширяемую систему метаданных. Новые блоки метаданных могут быть определены и реализованы в будущих версиях без потери обратной совместимости. Сейчас определены типы метаданных для таблиц поиска, тегов и списков разметки аудиодисков. Приложение может использовать блок метаданных &lt;tt&gt;APPLICATION&lt;/tt&gt; после регистрации для него &lt;tt&gt;ID&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поиск:&lt;/b&gt; FLAC поддерживает быстрый и точный поиск, что полезно не только при воспроизведении, но и дает возможность использовать FLAC в звуковых редакторах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поточность:&lt;/b&gt; Каждый фрейм FLAC содержит достаточно информации для собственного декодирования. Текущий фрейм FLAC не зависит от предыдущих и последующих. FLAC использует коды синхронизации и контрольные суммы, что позволяет декодеру быстро выбирать позицию в текущем потоке.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование:&lt;/b&gt; FLAC удобно использовать для архивации, так как сжатие с его помощью не приводит к потере информации. Если в будущем Вы решите использовать другой формат, данные будут восстановлены из .flac файла в первоначальном виде. Кроме контрольной суммы фрейма и подписи MD5, утилита &lt;tt&gt;flac&lt;/tt&gt; имеет возможность проверки, использование которой приводит к тому, что кодируемый поток сразу же декодируется и сравнивается с исходным. Если происходит ошибка, кодер прекращает работу.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование аудиодисков:&lt;/b&gt; У FLAC если блок метаданных &quot;cue sheet&quot;, в котором сохраняется таблица разметки аудиодиска. Например, можно записать аудиодиск в один файл, а затем импортировать его таблицу разметки при кодировании, чтобы полученный файл имел такое же представление как и диск. Если оригинал аудиодиска будет испорчен, то вы сможете восстановить таблицу разметки, чтобы записать точную копию диска.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Устойчивость от ошибок:&lt;/b&gt; Благодаря разбиению на фреймы, ошибки в потоке локализуются до уровня фрейма, в котором произошла ошибка (обычно несколько сотых секунды). В некоторых кодеках одна ошибка может привести к потере всего остатка потока.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;Чего &lt;b&gt;нет&lt;/b&gt; во FLAC:&lt;ul&gt;&lt;li&gt;FLAC не осуществляет сжатие с потерями. Для этого существует много хороших форматов, таких как Ogg Vorbis, MPC и MP3 (отличная реализация с открытими исходными текстами LAME).&lt;/li&gt;&lt;li&gt;FLAC не будет SDMI совместимым и т.п. Перед проектом не стоит цели поддерживать методы защиты, которые на практике лишь увеличивают объем файла. Конечно, мы не сможем препятствовать кому-либо создавать несвободные блоки метаданных, однако, стандартные декодеры все равно будут их пропускать.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Since FLAC is an open-source project, it&apos;s important to have a set of goals that everyone works to. They may change slightly from time to time but they&apos;re a good guideline. Changes should be in line with the goals and should not attempt to embrace any of the anti-goals.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC should be and stay an open format with an open-source reference implementation.&lt;/li&gt;&lt;li&gt;FLAC should be lossless. This seems obvious but lossy compression seems to creep into every audio codec. This goal also means that &lt;tt&gt;flac&lt;/tt&gt; should stay archival quality and be truly lossless for all input. Testing of releases should be thorough.&lt;/li&gt;&lt;li&gt;FLAC should yield respectable compression, on par or better than other lossless codecs.&lt;/li&gt;&lt;li&gt;FLAC should allow at least realtime decoding on even modest hardware.&lt;/li&gt;&lt;li&gt;FLAC should support fast sample-accurate seeking.&lt;/li&gt;&lt;li&gt;FLAC should allow gapless playback of consecutive streams. This follows from the lossless goal.&lt;/li&gt;&lt;li&gt;The FLAC project owes a lot to the many people who have advanced the audio compression field so freely, and aims also to contribute through the open-source development of new ideas.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Anti-goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Так как FLAC - это открытый проект, важно определить список целей, к чему нужно стремиться. Время от времени они могут немного изменяться, но всегда должны определять направление развития. изменения должны согласовываться с текущими целями и не пытаться включить в себя антицели.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Цели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC должен оставаться открытым форматом. Все исходные тексты либо под LGPL, либо под GPL.&lt;/li&gt;&lt;li&gt;FLAC должен производить только сжатие без потерь. Вроде бы это очевидно, однако, кодирование с потерями пытается проникнуть во все аудио кодеки. Эта цель также означает, что FLAC должен придерживаться только принципов архивирования и сжимать без потерь абсолютно все типы входных данных. Релизы должны тщательно тестироваться.&lt;/li&gt;&lt;li&gt;FLAC должен достичь приемлимого уровня сжатия файлов.&lt;/li&gt;&lt;li&gt;FLAC должен иметь низкие аппаратные требования и обеспечить декодирование в реальном времени даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать быстрый и точный поиск.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать воспроизведение без пауз для следующих друг за другом потоков.&lt;/li&gt;&lt;li&gt;Проект FLAC находится в долгу перед многими людьми, кто улучшал методы сжатия звука, и нацелен на поддержку новых идей с помощью открытой разработки.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Антицели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Сжатие с потерями. Существует достаточно много хороших форматов, предназначенных именно для этого (Ogg Vorbis, MP3, и т.д.).&lt;/li&gt;&lt;li&gt;Защита от копирования в любом виде.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC is a free codec in the fullest sense. This page explicitly states all that you may do with the format and software.&lt;/p&gt;&lt;p&gt;The FLAC and Ogg FLAC formats themselves, and their specifications, are fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance). They are free for commercial or noncommercial use. That means that commercial developers may independently write FLAC or Ogg FLAC software which is compatible with the specifications for no charge and without restrictions of any kind. There are no licensing fees of any kind for use of the formats or their specifications, or for distributing, selling, or streaming media in the FLAC or Ogg FLAC formats.&lt;/p&gt;&lt;p&gt;The FLAC project also makes available software that implements the formats, which is distributed according to Open Source licenses as follows:&lt;/p&gt;&lt;p&gt;The reference implementation libraries are licensed under Xiph&apos;s variant of the BSD License, a.k.a. the Xiph License. This is a trivial variation of the popular BSD License where the Xiph.org Foundation is substituted for the University of California, Berkeley. In simple terms, these libraries may be used by any application, Open or proprietary, linked or incorporated in whole, so long as acknowledgement is made to Xiph.org Foundation when using the source code in whole or in derived works. The Xiph License is free enough that the libraries have been used in commercial products to implement FLAC, including in the firmware of hardware devices where other Open Source licenses can be problematic. In the source code these libraries are called libFLAC and libFLAC++.&lt;/p&gt;&lt;p&gt;The rest of the software that the FLAC project provides is licensed under the GNU General Public License (GPL). This software includes various utilities for converting files to and from FLAC format, plugins for audio players, et cetera. In general, the GPL allows redistribution as long as derived works are also made available in source code form according to compatible terms.&lt;/p&gt;&lt;p&gt;Neither the FLAC nor Ogg FLAC formats nor any of the implemented encoding/decoding methods are covered by any known patent.&lt;/p&gt;&lt;p&gt;FLAC is one of a family of codecs of the Xiph.org Foundation, all created according to the same free ideals. For some other codecs&apos; descriptions of the Xiph License see the Speex and Vorbis license pages.&lt;/p&gt;&lt;p&gt;If you would like to redistribute parts or all of FLAC under different terms, contact &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Josh Coalson&lt;/a&gt;.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;...&lt;/p&gt;&lt;p&gt;FLAC - свободный кодек в самом полном смысле этого слова. Ниже описано то, что Вы можете сделать с форматом и программным обеспечением.&lt;/p&gt;&lt;p&gt;FLAC и Ogg FLAC форматы непосредственно, и их спецификации, являются полностью открытыми, могут использоваться для любой цели (проект FLAC оставляет за собой право установить спецификацию FLAC). Они бесплатные для коммерческого или некоммерческого использования. Это означает, что коммерческие разработчики могут независимо написать FLAC или Ogg FLAC программное обеспечение, которое является совместимым со спецификациями без ограничений любого вида. Нет никаких лицензионных сборов для использования форматов или их спецификаций, для распространения, продажи.&lt;/p&gt;&lt;p&gt;Ни FLAC, ни Ogg FLAC форматы, ни любой из осуществленных методов кодирования/расшифровки не закрыты никаким известным патентом.&lt;/p&gt;&lt;p&gt;Если Вы хотели бы распространить части или весь FLAC согласно другим условиям, свяжитесь с &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Джошем Коалсоном&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;...&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::AboutFlacDialog::AboutFlacDialogImpl</name>
    <message>
        <source>About FLAC</source>
        <translation type="obsolete">О FLAC</translation>
    </message>
    <message>
        <source>Features</source>
        <translation type="obsolete">Возможности</translation>
    </message>
    <message>
        <source>Notable features of FLAC</source>
        <translation type="obsolete">Известные особенности FLAC</translation>
    </message>
    <message>
        <source>Goals</source>
        <translation type="obsolete">Цели</translation>
    </message>
    <message>
        <source>Goals &amp; Anti-goals of FLAC</source>
        <translation type="obsolete">Цели и Антицели FLAC</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="obsolete">Лицензия</translation>
    </message>
    <message>
        <source>License of FLAC</source>
        <translation type="obsolete">Лицензия FLAC</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3, but lossless, meaning that audio is compressed in FLAC without any loss in quality. This is similar to how Zip works, except with FLAC you will get much better compression because it is designed specifically for audio, and you can play back compressed FLAC files in your favorite player (or your car or home stereo) just like you would an MP3 file.&lt;/p&gt;&lt;p&gt;FLAC supports tagging, cover art, and fast seeking. FLAC is freely available and supported on most operating systems, including Windows, &quot;unix&quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2, and Amiga.&lt;/p&gt;&lt;p&gt;When we say that FLAC is &quot;Free&quot; it means more than just that it is available at no cost. It means that the specification of the format is fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance), and that neither the FLAC format nor any of the implemented encoding/decoding methods are covered by any known patent. It also means that all the source code is available under open-source licenses. It is the first truly open and free lossless audio format.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;FLAC - это аббревиатура от Free Lossless Audio Codec (свободный аудиокодек, сжимающий без потерь). Проект FLAC включает:&lt;ul&gt;&lt;li&gt;потоковый формат;&lt;/li&gt;&lt;li&gt;базовые кодеры и декодеры в виде библиотек;&lt;/li&gt;&lt;li&gt;&lt;tt&gt;flac&lt;/tt&gt;, утилиту командной строки, выполняющую сжатие и распаковку файлов .flac;&lt;/li&gt;&lt;li&gt;&lt;tt&gt;metaflac&lt;/tt&gt;, утилиту командной строки для редактирования метаданных в файлах .flac;&lt;/li&gt;&lt;li&gt;плагины для разных плейеров.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;FLAC компилируется на множестве платформ: Windows, &quot;unix&quot; (Linux, *BSD, Solaris, OS X, IRIX), BeOS, OS/2 и Amiga. Имеются системы сборки для autoconf/automake, MSVC, Watcom C и Project Builder.&lt;/p&gt;&lt;p&gt;&quot;Свобода&quot; означает, что спецификация потокового формата открыта для всех и может быть использована для любых целей (проект FLAC оставляет за собой право устанавливать спецификации и сертифицировать относящиеся к нему продукты на совместимость), а также то, что ни формат, ни один из реализованных методов кодирования/декодирования не запатентованы. Это также значит, что все исходные тексты доступны по лицензиям, обязывающим предоставлять исходные коды.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Using</source>
        <translation type="obsolete">Используется</translation>
    </message>
    <message>
        <source>with support for Ogg FLAC</source>
        <translation type="obsolete">с поддержкой Ogg FLAC</translation>
    </message>
    <message>
        <source>without support for Ogg FLAC</source>
        <translation type="obsolete">без поддержки Ogg FLAC</translation>
    </message>
    <message>
        <source>See&lt;br&gt;&lt;a href=&quot;http://flac.sourceforge.net/&quot;&gt;flac.sourceforge.net&lt;/a&gt;&lt;br&gt;for more information.</source>
        <translation type="obsolete">См.&lt;br&gt;&lt;a href=&quot;http://flac.sourceforge.net/&quot;&gt;flac.sourceforge.net&lt;/a&gt;&lt;br&gt;для получения более подробной информации.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>FLAC: features</source>
        <translation type="obsolete">FLAC: возможности</translation>
    </message>
    <message>
        <source>&lt;p&gt;Notable features of FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Lossless:&lt;/b&gt; The encoding of audio (PCM) data incurs no loss of information, and the decoded audio is bit-for-bit identical to what went into the encoder. Each frame contains a 16-bit CRC of the frame data for detecting transmission errors. The integrity of the audio data is further insured by storing an MD5 signature of the original unencoded audio data in the file header, which can be compared against later during decoding or testing.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Fast:&lt;/b&gt; FLAC is asymmetric in favor of decode speed. Decoding requires only integer arithmetic, and is much less compute-intensive than for most perceptual codecs. Real-time decode performance is easily achievable on even modest hardware.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Hardware support:&lt;/b&gt; FLAC is supported by dozens of consumer electronic devices, from portable players, to home stereo equipment, to car stereo.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Flexible metadata:&lt;/b&gt; FLAC&apos;s metadata system supports tags, cover art, seek tables, and cue sheets. Applications can write their own APPLICATION metadata once they register an ID. New metadata blocks can be defined and implemented in future versions of FLAC without breaking older streams or decoders.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Seekable:&lt;/b&gt; FLAC supports fast sample-accurate seeking. Not only is this useful for playback, it makes FLAC files suitable for use in editing applications.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Streamable:&lt;/b&gt; Each FLAC frame contains enough data to decode that frame. FLAC does not even rely on previous or following frames. FLAC uses sync codes and CRCs (similar to MPEG and other formats), which, along with framing, allow decoders to pick up in the middle of a stream with a minimum of delay.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Suitable for archiving:&lt;/b&gt; FLAC is an open format, and there is no generation loss if you need to convert your data to another format in the future. In addition to the frame CRCs and MD5 signature, flac has a verify option that decodes the encoded stream in parallel with the encoding process and compares the result to the original, aborting with an error if there is a mismatch.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Convenient CD archiving:&lt;/b&gt; FLAC has a &quot;cue sheet&quot; metadata block for storing a CD table of contents and all track and index points. For instance, you can rip a CD to a single file, then import the CD&apos;s extracted cue sheet while encoding to yield a single file representation of the entire CD. If your original CD is damaged, the cue sheet can be exported later in order to burn an exact copy.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Error resistant:&lt;/b&gt; Because of FLAC&apos;s framing, stream errors limit the damage to the frame in which the error occurred, typically a small fraction of a second worth of data. Contrast this with some other lossless codecs, in which a single error destroys the remainder of the stream.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;What FLAC is &lt;b&gt;not&lt;/b&gt;:&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Характеристики FLAC:&lt;ul&gt;&lt;li&gt;&lt;b&gt;Сжатие без потерь:&lt;/b&gt; Кодирование PCM данных не приводит к потере информации, следовательно декодируемый аудиофайл абсолютно идентичен тому, который был подан на вход кодеру. Чтобы определить возможные ошибки при передаче файла, для каждого фрейма вычисляется 16-битная контрольная сумма. Целостность на дальнейшем этапе подтверждается подписью MD5 распакованных данных, которая находится в заголовке и может быть проверена при воспроизведении, декодировании или с помощью тестирования.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Скорость:&lt;/b&gt; Скорость работы при кодировании и декодировании несимметричны. Для декодирования используется только целочисленная арифметика, которая требует значительно меньше вычислений, чем в перцепционных кодеках. Декодирование в реальном времени легко достижимо даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Аппаратная поддержка:&lt;/b&gt; Благодаря свободной базовой реализации и простому декодированию FLAC является единственным аудиокодеком, сжимающим без потерь, который имеет аппаратную поддержку.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Метаданные:&lt;/b&gt; FLAC имеет расширяемую систему метаданных. Новые блоки метаданных могут быть определены и реализованы в будущих версиях без потери обратной совместимости. Сейчас определены типы метаданных для таблиц поиска, тегов и списков разметки аудиодисков. Приложение может использовать блок метаданных &lt;tt&gt;APPLICATION&lt;/tt&gt; после регистрации для него &lt;tt&gt;ID&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поиск:&lt;/b&gt; FLAC поддерживает быстрый и точный поиск, что полезно не только при воспроизведении, но и дает возможность использовать FLAC в звуковых редакторах.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Поточность:&lt;/b&gt; Каждый фрейм FLAC содержит достаточно информации для собственного декодирования. Текущий фрейм FLAC не зависит от предыдущих и последующих. FLAC использует коды синхронизации и контрольные суммы, что позволяет декодеру быстро выбирать позицию в текущем потоке.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование:&lt;/b&gt; FLAC удобно использовать для архивации, так как сжатие с его помощью не приводит к потере информации. Если в будущем Вы решите использовать другой формат, данные будут восстановлены из .flac файла в первоначальном виде. Кроме контрольной суммы фрейма и подписи MD5, утилита &lt;tt&gt;flac&lt;/tt&gt; имеет возможность проверки, использование которой приводит к тому, что кодируемый поток сразу же декодируется и сравнивается с исходным. Если происходит ошибка, кодер прекращает работу.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Архивирование аудиодисков:&lt;/b&gt; У FLAC если блок метаданных &quot;cue sheet&quot;, в котором сохраняется таблица разметки аудиодиска. Например, можно записать аудиодиск в один файл, а затем импортировать его таблицу разметки при кодировании, чтобы полученный файл имел такое же представление как и диск. Если оригинал аудиодиска будет испорчен, то вы сможете восстановить таблицу разметки, чтобы записать точную копию диска.&lt;/li&gt;&lt;li&gt;&lt;b&gt;Устойчивость от ошибок:&lt;/b&gt; Благодаря разбиению на фреймы, ошибки в потоке локализуются до уровня фрейма, в котором произошла ошибка (обычно несколько сотых секунды). В некоторых кодеках одна ошибка может привести к потере всего остатка потока.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;Чего &lt;b&gt;нет&lt;/b&gt; во FLAC:&lt;ul&gt;&lt;li&gt;FLAC не осуществляет сжатие с потерями. Для этого существует много хороших форматов, таких как Ogg Vorbis, MPC и MP3 (отличная реализация с открытими исходными текстами LAME).&lt;/li&gt;&lt;li&gt;FLAC не будет SDMI совместимым и т.п. Перед проектом не стоит цели поддерживать методы защиты, которые на практике лишь увеличивают объем файла. Конечно, мы не сможем препятствовать кому-либо создавать несвободные блоки метаданных, однако, стандартные декодеры все равно будут их пропускать.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>FLAC: goals</source>
        <translation type="obsolete">FLAC: цели</translation>
    </message>
    <message>
        <source>&lt;p&gt;Since FLAC is an open-source project, it&apos;s important to have a set of goals that everyone works to. They may change slightly from time to time but they&apos;re a good guideline. Changes should be in line with the goals and should not attempt to embrace any of the anti-goals.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC should be and stay an open format with an open-source reference implementation.&lt;/li&gt;&lt;li&gt;FLAC should be lossless. This seems obvious but lossy compression seems to creep into every audio codec. This goal also means that &lt;tt&gt;flac&lt;/tt&gt; should stay archival quality and be truly lossless for all input. Testing of releases should be thorough.&lt;/li&gt;&lt;li&gt;FLAC should yield respectable compression, on par or better than other lossless codecs.&lt;/li&gt;&lt;li&gt;FLAC should allow at least realtime decoding on even modest hardware.&lt;/li&gt;&lt;li&gt;FLAC should support fast sample-accurate seeking.&lt;/li&gt;&lt;li&gt;FLAC should allow gapless playback of consecutive streams. This follows from the lossless goal.&lt;/li&gt;&lt;li&gt;The FLAC project owes a lot to the many people who have advanced the audio compression field so freely, and aims also to contribute through the open-source development of new ideas.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Anti-goals&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Lossy compression. There are already many suitable lossy formats (Ogg Vorbis, MP3, etc.).&lt;/li&gt;&lt;li&gt;Copy prevention, DRM, etc. There is no intention to add any copy prevention methods. Of course, we can&apos;t stop someone from encrypting a FLAC stream in another container (e.g. the way Apple encrypts AAC in MP4 with FairPlay), that is the choice of the user.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Так как FLAC - это открытый проект, важно определить список целей, к чему нужно стремиться. Время от времени они могут немного изменяться, но всегда должны определять направление развития. изменения должны согласовываться с текущими целями и не пытаться включить в себя антицели.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Цели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;FLAC должен оставаться открытым форматом. Все исходные тексты либо под LGPL, либо под GPL.&lt;/li&gt;&lt;li&gt;FLAC должен производить только сжатие без потерь. Вроде бы это очевидно, однако, кодирование с потерями пытается проникнуть во все аудио кодеки. Эта цель также означает, что FLAC должен придерживаться только принципов архивирования и сжимать без потерь абсолютно все типы входных данных. Релизы должны тщательно тестироваться.&lt;/li&gt;&lt;li&gt;FLAC должен достичь приемлимого уровня сжатия файлов.&lt;/li&gt;&lt;li&gt;FLAC должен иметь низкие аппаратные требования и обеспечить декодирование в реальном времени даже на старых компьютерах.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать быстрый и точный поиск.&lt;/li&gt;&lt;li&gt;FLAC должен поддерживать воспроизведение без пауз для следующих друг за другом потоков.&lt;/li&gt;&lt;li&gt;Проект FLAC находится в долгу перед многими людьми, кто улучшал методы сжатия звука, и нацелен на поддержку новых идей с помощью открытой разработки.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Антицели&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Сжатие с потерями. Существует достаточно много хороших форматов, предназначенных именно для этого (Ogg Vorbis, MP3, и т.д.).&lt;/li&gt;&lt;li&gt;Защита от копирования в любом виде.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>FLAC: license</source>
        <translation type="obsolete">FLAC: лицензия</translation>
    </message>
    <message>
        <source>&lt;p&gt;FLAC is a free codec in the fullest sense. This page explicitly states all that you may do with the format and software.&lt;/p&gt;&lt;p&gt;The FLAC and Ogg FLAC formats themselves, and their specifications, are fully open to the public to be used for any purpose (the FLAC project reserves the right to set the FLAC specification and certify compliance). They are free for commercial or noncommercial use. That means that commercial developers may independently write FLAC or Ogg FLAC software which is compatible with the specifications for no charge and without restrictions of any kind. There are no licensing fees of any kind for use of the formats or their specifications, or for distributing, selling, or streaming media in the FLAC or Ogg FLAC formats.&lt;/p&gt;&lt;p&gt;The FLAC project also makes available software that implements the formats, which is distributed according to Open Source licenses as follows:&lt;/p&gt;&lt;p&gt;The reference implementation libraries are licensed under Xiph&apos;s variant of the BSD License, a.k.a. the Xiph License. This is a trivial variation of the popular BSD License where the Xiph.org Foundation is substituted for the University of California, Berkeley. In simple terms, these libraries may be used by any application, Open or proprietary, linked or incorporated in whole, so long as acknowledgement is made to Xiph.org Foundation when using the source code in whole or in derived works. The Xiph License is free enough that the libraries have been used in commercial products to implement FLAC, including in the firmware of hardware devices where other Open Source licenses can be problematic. In the source code these libraries are called libFLAC and libFLAC++.&lt;/p&gt;&lt;p&gt;The rest of the software that the FLAC project provides is licensed under the GNU General Public License (GPL). This software includes various utilities for converting files to and from FLAC format, plugins for audio players, et cetera. In general, the GPL allows redistribution as long as derived works are also made available in source code form according to compatible terms.&lt;/p&gt;&lt;p&gt;Neither the FLAC nor Ogg FLAC formats nor any of the implemented encoding/decoding methods are covered by any known patent.&lt;/p&gt;&lt;p&gt;FLAC is one of a family of codecs of the Xiph.org Foundation, all created according to the same free ideals. For some other codecs&apos; descriptions of the Xiph License see the Speex and Vorbis license pages.&lt;/p&gt;&lt;p&gt;If you would like to redistribute parts or all of FLAC under different terms, contact &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Josh Coalson&lt;/a&gt;.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;...&lt;/p&gt;&lt;p&gt;FLAC - свободный кодек в самом полном смысле этого слова. Ниже описано то, что Вы можете сделать с форматом и программным обеспечением.&lt;/p&gt;&lt;p&gt;FLAC и Ogg FLAC форматы непосредственно, и их спецификации, являются полностью открытыми, могут использоваться для любой цели (проект FLAC оставляет за собой право установить спецификацию FLAC). Они бесплатные для коммерческого или некоммерческого использования. Это означает, что коммерческие разработчики могут независимо написать FLAC или Ogg FLAC программное обеспечение, которое является совместимым со спецификациями без ограничений любого вида. Нет никаких лицензионных сборов для использования форматов или их спецификаций, для распространения, продажи.&lt;/p&gt;&lt;p&gt;Ни FLAC, ни Ogg FLAC форматы, ни любой из осуществленных методов кодирования/расшифровки не закрыты никаким известным патентом.&lt;/p&gt;&lt;p&gt;Если Вы хотели бы распространить части или весь FLAC согласно другим условиям, свяжитесь с &lt;a href=&quot;mailto:jcoalson@users.sourceforge.net&quot;&gt;Джошем Коалсоном&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;...&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Goals and Anti-goals of FLAC</source>
        <translation type="obsolete">Цели и Антицели FLAC</translation>
    </message>
    <message>
        <source>See&lt;br&gt;%1&lt;br&gt;for more information.</source>
        <translation type="obsolete">См.&lt;br&gt;%1&lt;br&gt;для получения более подробной информации.</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::ApplicationErrorDialog</name>
    <message>
        <source>&lt;p&gt;Internal program error.&lt;/p&gt;&lt;p&gt;Please, send the error report to the developer. You will help to make this program better.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Внутренняя ошибка в программе.&lt;/p&gt;&lt;p&gt;Пожалуйста, отошлите отчёт об ошибке разработчику. Вы поможете сделать эту программу лучше.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Terminate</source>
        <translation type="obsolete">Завершить</translation>
    </message>
    <message>
        <source>Save error report</source>
        <translation type="obsolete">Сохранить отчёт об ошибке</translation>
    </message>
    <message>
        <source>Can&apos;t save error report file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно сохранить файл отчёта об ошибке &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::ApplicationErrorDialog::ApplicationErrorDialogImpl</name>
    <message>
        <source>Can&apos;t save error report file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно сохранить файл отчёта об ошибке &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::MainWindow</name>
    <message>
        <source>Can&apos;t install language &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно установить язык &apos;%1&apos;</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation type="obsolete">&amp;Выход</translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation type="obsolete">Выйти из приложения</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="obsolete">&amp;О программе</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="obsolete">&amp;Файл</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="obsolete">&amp;Вид</translation>
    </message>
    <message>
        <source>&amp;Language</source>
        <translation type="obsolete">&amp;Язык</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Помощь</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>About FLAC</source>
        <translation type="obsolete">О библиотеке FLAC</translation>
    </message>
    <message>
        <source>with support for Ogg FLAC</source>
        <translation type="obsolete">с поддержкой Ogg FLAC</translation>
    </message>
    <message>
        <source>without support for Ogg FLAC</source>
        <translation type="obsolete">без поддержки Ogg FLAC</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::MainWindow::MainWindowImpl</name>
    <message>
        <source>E&amp;xit</source>
        <translation type="obsolete">&amp;Выход</translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation type="obsolete">Выйти из приложения</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="obsolete">&amp;О программе</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="obsolete">&amp;Файл</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="obsolete">&amp;Вид</translation>
    </message>
    <message>
        <source>&amp;Language</source>
        <translation type="obsolete">&amp;Язык</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Помощь</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="obsolete">Готов</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>About FLAC</source>
        <translation type="obsolete">О библиотеке FLAC</translation>
    </message>
    <message>
        <source>with support for Ogg FLAC</source>
        <translation type="obsolete">с поддержкой Ogg FLAC</translation>
    </message>
    <message>
        <source>without support for Ogg FLAC</source>
        <translation type="obsolete">без поддержки Ogg FLAC</translation>
    </message>
    <message>
        <source>&amp;About...</source>
        <translation type="obsolete">&amp;О программе...</translation>
    </message>
    <message>
        <source>About &amp;FLAC...</source>
        <translation type="obsolete">О &amp;FLAC...</translation>
    </message>
    <message>
        <source>About &amp;Qt...</source>
        <translation type="obsolete">О &amp;Qt...</translation>
    </message>
    <message>
        <source>Change language to &apos;%1&apos;</source>
        <translation type="obsolete">Изменить язык на &apos;%1&apos;</translation>
    </message>
    <message>
        <source>&amp;Program log...</source>
        <translation type="obsolete">&amp;Лог программы...</translation>
    </message>
    <message>
        <source>Show program log</source>
        <translation type="obsolete">Показать лог программы</translation>
    </message>
    <message>
        <source>Show information about the application</source>
        <translation type="obsolete">Показать информацию о программе</translation>
    </message>
    <message>
        <source>Show information about FLAC</source>
        <translation type="obsolete">Показать информацию о FLAC</translation>
    </message>
    <message>
        <source>Show information about Qt</source>
        <translation type="obsolete">Показать информацию о Qt</translation>
    </message>
    <message>
        <source>&amp;Action</source>
        <translation type="obsolete">&amp;Действие</translation>
    </message>
    <message>
        <source>Program log</source>
        <translation type="obsolete">Лог программы</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <comment>Quit</comment>
        <translation type="obsolete">Ctrl+Q</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::TextBrowserDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>QFLAC::Window::TextViewerDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="32"/>
        <source>No error occurred</source>
        <translation>Нет ошибок</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="34"/>
        <source>An error occurred when reading from the file</source>
        <translation>Невозможно прочитать файл</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="36"/>
        <source>An error occurred when writing to the file</source>
        <translation>Невозможно записать файл</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="38"/>
        <source>A fatal error occurred</source>
        <translation>Несправимая ошибка</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="40"/>
        <source>A resource error occurred</source>
        <translation>Не найден ресурс или к нему нет доступа</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="42"/>
        <source>The file could not be opened</source>
        <translation>Невозможно открыть файл</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="44"/>
        <source>The operation was aborted</source>
        <translation>Процесс был отменён</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="46"/>
        <source>A timeout occurred</source>
        <translation>Ограничение по времени</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="48"/>
        <source>An unspecified error occurred</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="50"/>
        <source>The file could not be removed</source>
        <translation>Невозможно удалить файл</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="52"/>
        <source>The file could not be renamed</source>
        <translation>Невозможно переименовать файл</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="54"/>
        <source>The position in the file could not be changed</source>
        <translation>Невозможно переместить файловый указатель для произвольного доступа</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="56"/>
        <source>The file could not be resized</source>
        <translation>Невозможно изменить размеры файла</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="58"/>
        <source>The file could not be accessed</source>
        <translation>Невозможно получить доступ к файлу</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="60"/>
        <source>The file could not be copied</source>
        <translation>Невозможно скопировать файл</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="63"/>
        <source>Unsupported Qt version</source>
        <translation>Неподдерживаемая версия Qt</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="74"/>
        <source>Can&apos;t open file &apos;%1&apos; (%2: %3)</source>
        <translation>Невозможно открыть файл &apos;%1&apos; (%2: %3)</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="82"/>
        <source>Can&apos;t read file &apos;%1&apos; (%2: %3)</source>
        <translation>Невозможно прочитать файл &apos;%1&apos; (%2: %3)</translation>
    </message>
    <message>
        <location filename="../../sources/filewrapper.cpp" line="91"/>
        <source>Can&apos;t write file &apos;%1&apos; (%2: %3)</source>
        <translation>Невозможно записать файл &apos;%1&apos; (%2: %3)</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../../sources/service.cpp" line="63"/>
        <source>Fatal error</source>
        <translation>Несправимая ошибка</translation>
    </message>
    <message>
        <source>Internal program error</source>
        <translation type="vanished">Внутренняя ошибка в программе</translation>
    </message>
    <message>
        <source>Unexpected exception</source>
        <translation type="vanished">Неожиданное исключение</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="../../sources/service.cpp" line="32"/>
        <source>Internal error</source>
        <translation>Внутренняя ошибка</translation>
    </message>
    <message>
        <location filename="../../sources/service.cpp" line="39"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../../sources/service.cpp" line="45"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../../sources/service.cpp" line="51"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../sources/service.cpp" line="57"/>
        <source>Critical error</source>
        <translation>Критическая ошибка</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Audio Converter</source>
        <translation type="vanished">Конвертер аудио файлов</translation>
    </message>
    <message>
        <location filename="../../sources/info.cpp" line="31"/>
        <source>Media Converter</source>
        <translation>Конвертер мультимедиа файлов</translation>
    </message>
    <message>
        <location filename="../../sources/info.cpp" line="61"/>
        <source>Yanis Kurganov</source>
        <translation>Янис Курганов</translation>
    </message>
    <message>
        <location filename="../../sources/info.cpp" line="86"/>
        <source>Media files (%1)</source>
        <translation>Мультимедиа файлы (%1)</translation>
    </message>
    <message>
        <location filename="../../sources/info.cpp" line="86"/>
        <location filename="../../sources/info.cpp" line="91"/>
        <source>All files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../../sources/info.cpp" line="101"/>
        <source>Profile files (%1)</source>
        <translation>Профили (%1)</translation>
    </message>
    <message>
        <source>Media Files (%1)</source>
        <translation type="vanished">Мультимедиа файлы (%1)</translation>
    </message>
    <message>
        <source>Profile Files (%1)</source>
        <translation type="vanished">Профили (%1)</translation>
    </message>
    <message>
        <source>Audio Files (%1)</source>
        <translation type="vanished">Аудио файлы (%1)</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation type="vanished">Все файлы (*)</translation>
    </message>
    <message>
        <source>Preset Files (%1)</source>
        <translation type="vanished">Файлы настроек (%1)</translation>
    </message>
    <message>
        <source>Can&apos;t allocate archive</source>
        <translation type="vanished">Невозможно выделить память для объекта архива</translation>
    </message>
    <message>
        <location filename="../../sources/archive.cpp" line="42"/>
        <source>Can&apos;t allocate archive object</source>
        <translation>Невозможно выделить память для объекта архива</translation>
    </message>
    <message>
        <location filename="../../sources/archive.cpp" line="69"/>
        <source>Error opening archive: %1 (%2)</source>
        <translation>Ошибка при открытии архива: %1 (%2)</translation>
    </message>
    <message>
        <location filename="../../sources/archive.cpp" line="83"/>
        <source>Error fetching archive header: %1 (%2)</source>
        <translation>Ошибка при обработке заголовка архива: %1 (%2)</translation>
    </message>
</context>
<context>
    <name>QSettings</name>
    <message>
        <location filename="../../sources/settingsmanager.cpp" line="36"/>
        <source>Settings are not writable</source>
        <translation>Настройки программы доступны только для чтения</translation>
    </message>
    <message>
        <location filename="../../sources/settingsmanager.cpp" line="43"/>
        <location filename="../../sources/settingsmanager.cpp" line="46"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../../sources/settingsmanager.cpp" line="43"/>
        <source>an access error occurred</source>
        <translation>нет доступа</translation>
    </message>
    <message>
        <location filename="../../sources/settingsmanager.cpp" line="46"/>
        <source>a format error occurred</source>
        <translation>неверный формат</translation>
    </message>
    <message>
        <location filename="../../sources/settingsmanager.cpp" line="50"/>
        <source>Unsupported version of the Qt library</source>
        <translation>Неподдерживаемая версия Qt</translation>
    </message>
    <message>
        <location filename="../../sources/settingsmanager.cpp" line="60"/>
        <source>Unsupported version of the program options</source>
        <translation>Неподдерживаемая версия настроек программы</translation>
    </message>
    <message>
        <source>An access error occurred</source>
        <translation type="vanished">Нет доступа</translation>
    </message>
    <message>
        <source>A format error occurred</source>
        <translation type="vanished">Неверный формат</translation>
    </message>
    <message>
        <source>Unsupported Qt version</source>
        <translation type="vanished">Неподдерживаемая версия Qt</translation>
    </message>
    <message>
        <source>Plugin directory is &apos;%1&apos;</source>
        <translation type="vanished">Директория с плагинами &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../sources/settingsmanager.cpp" line="68"/>
        <source>Language directory is &apos;%1&apos;</source>
        <translation>Директория с языковыми файлами &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <source>Ready</source>
        <comment>Default message of the status bar</comment>
        <translation type="obsolete">Готов</translation>
    </message>
</context>
<context>
    <name>TextBrowserDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
</TS>
