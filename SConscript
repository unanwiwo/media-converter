# -*- coding: utf-8 -*-

# This file is part of Media Converter.
#
# Media Converter is an open source, cross-platform GUI front-end for various media codecs and command-line tools.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import utils, os
Import("env")

env = env.Clone(
    tools=["qt", "settings", "sign", "rcx"],
)

env.PrependUnique(
    CPPPATH="sources",
)

env.VariantDir("$SCONSX_OBJECT_BUILD_PATH", "sources", duplicate=False)

sources = [os.path.join("$SCONSX_OBJECT_BUILD_PATH", node.name) for node in utils.filter_sources(env, env.Glob(os.path.join("sources", "*.cpp")))]
settings = env.Glob(os.path.join("settings", "*.xsds"))
forms = env.Glob(os.path.join("forms", "*.ui"))
resources = env.Glob(os.path.join("resources", "*.*rc*"))

targets = [
    env.Sign(
        os.path.join("$SCONSX_BINARY_BUILD_PATH", "media-converter"),
        env.Program(
            os.path.join("$SCONSX_BINARY_BUILD_PATH", "media-converter-unsigned"),
            sources + settings + forms + resources
        )
    )
]

Return("targets")
