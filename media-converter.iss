[Setup]
AppId={{2C81B8BD-4D0D-488B-806B-34FC7C440E61}
AppName={#INNO_NAME}
AppVersion={#INNO_VERSION}
AppPublisher={#INNO_PUBLISHER}
AppPublisherURL={#INNO_URL}
AppSupportURL={#INNO_URL}
AppUpdatesURL={#INNO_URL}
DefaultDirName={userpf}\{#INNO_NAME}
LicenseFile=LICENSE
InfoBeforeFile=README
Compression=lzma
SolidCompression=yes
DisableProgramGroupPage=yes
PrivilegesRequired=lowest

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"; LicenseFile: "LICENSE.RUS"; InfoBeforeFile: "README.RUS"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Files]
Source: "LICENSE"; DestDir: "{app}"; Flags: ignoreversion
Source: "LICENSE.RUS"; DestDir: "{app}"; Flags: ignoreversion
Source: "README"; DestDir: "{app}"; Flags: ignoreversion
Source: "README.RUS"; DestDir: "{app}"; Flags: ignoreversion
Source: "presets\*"; DestDir: "{app}\presets"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\{#INNO_MAIN_EXE_NAME}"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\platforms\*"; DestDir: "{app}\platforms"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\languages\*"; DestDir: "{app}\languages"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libarchive-13.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libbz2-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libexpat-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libfreetype-6.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libgcc_s_seh-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libglib-2.0-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libgraphite2.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libharfbuzz-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libiconv-2.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libicudt62.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libicuin62.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libicuuc62.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libintl-8.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\liblz4.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\liblzma-5.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libnettle-6.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libpcre-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libpcre2-16-0.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libpng16-16.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libstdc++-6.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libwinpthread-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\libzstd.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#INNO_SOURCE_PATH}\zlib1.dll"; DestDir: "{app}"; Flags: ignoreversion

[Registry]
Root: HKCU; Subkey: "Software\yanis-kurganov\media-converter"; ValueType: string; ValueName: "LanguagePath"; ValueData: "{app}\languages"; Flags: noerror uninsdeletevalue

[Icons]
Name: "{userprograms}\{#INNO_NAME}"; Filename: "{app}\{#INNO_MAIN_EXE_NAME}"
Name: "{userdesktop}\{#INNO_NAME}"; Filename: "{app}\{#INNO_MAIN_EXE_NAME}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#INNO_MAIN_EXE_NAME}"; Description: "{cm:LaunchProgram,{#StringChange(INNO_NAME, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
